# -*- mode:python; coding: utf-8 -*-
# vim: set ft=python:
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# ------------------------------------------------------------------------------
# Imports
# ------------------------------------------------------------------------------
from __future__ import print_function

import sys
import os

from subprocess import check_output
from warnings import warn

# Import below not strictly necessary, but good for pep8
from SCons.Script import (
    EnsurePythonVersion,
    EnsureSConsVersion,
    Help,
    Environment,
    Variables,
    EnumVariable,
    PathVariable,
    BoolVariable,
    ListVariable,
    Split,
    Export,
)

from SCons.Errors import StopError
from SCons import __version__ as scons_version

from version import get_git_subst
from detect import (FindFFTW, FindBoost, FindThrust, FindCuda, FindExpolit,
                    FindPybind11, FindLibCUDACXX, FindPETSc)

from INFOS import TAMAAS_INFOS

# ------------------------------------------------------------------------------

EnsurePythonVersion(3, 6)
EnsureSConsVersion(3, 0)


# ------------------------------------------------------------------------------
def detect_dependencies(env):
    "Detect all dependencies"
    fftw_comp = {
        'omp': ['omp'],
        'threads': ['threads'],
        'none': [],
    }

    fftw_components = fftw_comp[env['fftw_threads']]
    if main_env['use_mpi']:
        fftw_components.append('mpi')

    if main_env['use_fftw']:
        FindFFTW(env, fftw_components, precision=env['real_type'])
    if main_env['use_cuda']:
        FindCuda(env)

    FindBoost(env, [
        'boost/version.hpp',
        'boost/preprocessor/seq.hpp',
        'boost/variant.hpp',
    ])

    # Use thrust shipped with cuda if cuda is requested
    thrust_var = 'CUDA_ROOT' if env['use_cuda'] else 'THRUST_ROOT'
    FindThrust(env, env['backend'], thrust_var)
    FindLibCUDACXX(env, "LIBCUDACXX_ROOT")

    if env['build_python']:
        FindPybind11(env)

    if env['use_petsc']:
        FindPETSc(env)

    FindExpolit(env)


def subdir(env, dir):
    "Building a sub-directory"
    return env.SConscript(env.File('SConscript', dir),
                          variant_dir=env.Dir(dir, env['build_dir']),
                          duplicate=True)


def print_build_info(env):
    info = ("-- Tamaas ${version}\n" + "-- SCons {} (Python {}.{})\n".format(
        scons_version, sys.version_info.major, sys.version_info.minor) +
            "-- Build type: ${build_type}\n" +
            "-- Thrust backend: ${backend}\n" +
            ("-- FFTW threads: ${fftw_threads}\n" if env['use_fftw'] else '') +
            "-- MPI: ${use_mpi}\n" + "-- PETSc: ${use_petsc}\n" +
            "-- Build directory: ${build_dir}\n" +
            ("-- Python version (bindings): $py_version"
             if env['build_python'] else ''))
    print(env.subst(info))


# ------------------------------------------------------------------------------
# Main compilation
# ------------------------------------------------------------------------------

# Compilation colors
colors = {
    'cyan': '\033[96m',
    'purple': '\033[95m',
    'blue': '\033[94m',
    'green': '\033[92m',
    'yellow': '\033[93m',
    'gray': '\033[38;5;8m',
    'orange': '\033[38;5;208m',
    'red': '\033[91m',
    'end': '\033[0m'
}

# Inherit all environment variables (for CXX detection, etc.)
main_env = Environment(ENV=os.environ, )

# Set tamaas information
for k, v in TAMAAS_INFOS._asdict().items():
    main_env[k] = v

main_env['COLOR_DICT'] = colors
main_env.AddMethod(subdir, 'SubDirectory')

# Build variables
vars = Variables('build-setup.conf')
vars.AddVariables(
    EnumVariable('build_type',
                 'Build type',
                 'release',
                 allowed_values=('release', 'profiling', 'debug'),
                 ignorecase=2),
    EnumVariable('backend',
                 'Thrust backend',
                 'cpp',
                 allowed_values=('cpp', 'omp', 'tbb', 'cuda'),
                 ignorecase=2),
    EnumVariable('fftw_threads',
                 'Threads FFTW library preference',
                 'none',
                 allowed_values=('omp', 'threads', 'none'),
                 ignorecase=2),
    EnumVariable('sanitizer',
                 'Sanitizer type',
                 'none',
                 allowed_values=('none', 'memory', 'leaks', 'address'),
                 ignorecase=2),
    PathVariable('prefix', 'Prefix where to install', '/usr/local',
                 PathVariable.PathAccept),

    # Dependencies paths
    PathVariable('FFTW_ROOT', 'FFTW custom path', os.getenv('FFTW_ROOT', ''),
                 PathVariable.PathAccept),
    PathVariable('THRUST_ROOT', 'Thrust custom path',
                 os.getenv('THRUST_ROOT', ''), PathVariable.PathAccept),
    PathVariable('LIBCUDACXX_ROOT', 'libcudacxx custom path',
                 os.getenv('LIBCUDACXX_ROOT', ''), PathVariable.PathAccept),
    PathVariable('BOOST_ROOT', 'Boost custom path',
                 os.getenv('BOOST_ROOT', ''), PathVariable.PathAccept),
    PathVariable('CUDA_ROOT', 'Cuda custom path', os.getenv('CUDA_ROOT', ''),
                 PathVariable.PathAccept),
    PathVariable('GTEST_ROOT', 'Googletest custom path',
                 os.getenv('GTEST_ROOT', ''), PathVariable.PathAccept),
    PathVariable('PYBIND11_ROOT', 'Pybind11 custom path',
                 os.getenv('PYBIND11_ROOT', ''), PathVariable.PathAccept),
    PathVariable('PETSC_ROOT', 'PETSc custom path',
                 os.getenv('PETSC_ROOT', ''), PathVariable.PathAccept),

    # Executables
    ('CXX', 'Compiler', os.getenv('CXX', 'g++')),
    ('MPICXX', 'MPI Compiler wrapper', os.getenv('MPICXX', 'mpicxx')),
    ('py_exec', 'Python executable', 'python3'),

    # Compiler flags
    ('CXXFLAGS', 'C++ compiler flags', os.getenv('CXXFLAGS', "")),

    # Pip flags
    ('PIPFLAGS', 'Pip installation flags', ""),

    # Cosmetic
    BoolVariable('verbose', 'Activate verbosity',
                 os.getenv('VERBOSE') in {'1', 'True', 'true'}),
    BoolVariable('color', 'Color the non-verbose compilation output', False),

    # Tamaas components
    BoolVariable('build_tests', 'Build test suite', False),
    BoolVariable('build_python', 'Build python wrapper', True),

    # Documentation
    ListVariable('doc_builders',
                 'Generated documentation formats',
                 default='html',
                 names=Split("html man")),  # TODO include latex

    # Dependencies
    BoolVariable('use_mpi', 'Builds multi-process parallelism', False),
    BoolVariable('use_petsc', 'Builds a PETSc solver wrapper', False),

    # Distribution options
    BoolVariable('strip_info', 'Strip binary of added information', True),
    BoolVariable('build_static_lib', "Build a static libTamaas", False),

    # Type variables
    EnumVariable('real_type',
                 'Type for real precision variables',
                 'double',
                 allowed_values=('double', 'long double')),
    EnumVariable('integer_type',
                 'Type for integer variables',
                 'int',
                 allowed_values=('int', 'long')),
)

# Set variables of environment
vars.Update(main_env)
help_text = vars.GenerateHelpText(main_env)
help_text += """
Commands:
    scons [build] [options]...                            Compile Tamaas (and additional modules/tests)
    scons install [prefix=/your/prefix] [options]...      Install Tamaas to prefix
    scons dev                                             Install symlink to Tamaas python module (useful to development purposes)
    scons test                                            Run tests with pytest
    scons doc                                             Compile documentation with Doxygen and Sphinx+Breathe
    scons archive                                         Create a gzipped archive from source
"""  # noqa
Help(help_text)

# Save all options, not just those that differ from default
with open('build-setup.conf', 'w') as setup:
    for option in vars.options:
        setup.write("# " + option.help.replace('\n', '\n# ') + "\n")
        setup.write("{} = '{}'\n".format(option.key, main_env[option.key]))

# Printing unknown variables
if vars.UnknownVariables():
    warn(f'unknown variables provided:\n\t{vars.UnknownVariables()}')

main_env['should_configure'] = \
    not main_env.GetOption('clean') and not main_env.GetOption('help')

build_type = main_env['build_type']
build_dir = 'build-${build_type}'
main_env['build_dir'] = main_env.Dir(build_dir)

# Setting up the python name with version
if main_env['build_python']:
    args = (main_env.subst("${py_exec} -c").split() + [
        "from sysconfig import get_python_version;"
        "print(get_python_version())"
    ])
    main_env['py_version'] = bytes(check_output(args)).decode()

verbose = main_env['verbose']

# Remove colors if not set
if not main_env['color']:
    for key in colors:
        colors[key] = ''

if not verbose:
    main_env['CXXCOMSTR'] = main_env['SHCXXCOMSTR'] = \
        u'{0}[Compiling ($SHCXX)] {1}$SOURCE'.format(colors['green'],
                                                     colors['end'])
    main_env['LINKCOMSTR'] = main_env['SHLINKCOMSTR'] = \
        u'{0}[Linking] {1}$TARGET'.format(colors['purple'],
                                          colors['end'])
    main_env['ARCOMSTR'] = u'{}[Ar]{} $TARGET'.format(colors['purple'],
                                                      colors['end'])
    main_env['RANLIBCOMSTR'] = \
        u'{}[Randlib]{} $TARGET'.format(colors['purple'],
                                        colors['end'])
    main_env['PRINT_CMD_LINE_FUNC'] = pretty_cmd_print
    main_env['INSTALLSTR'] = \
        u'{}[Installing] {}$SOURCE to $TARGET'.format(colors['blue'],
                                                      colors['end'])

# Include paths
main_env.AppendUnique(CPPPATH=[
    '#/src', '#/src/core', '#/src/surface', '#/src/percolation', '#/src/model',
    '#/src/solvers', '#/src/gpu', '#/python', '#/third-party/expolit/include'
])

# Changing the shared object extension
main_env['SHOBJSUFFIX'] = '.o'

# Variables for clarity
main_env['use_cuda'] = main_env['backend'] == "cuda"
main_env['use_fftw'] = not main_env['use_cuda']
main_env['use_mpi'] = main_env['use_mpi'] and not main_env['use_cuda']

if not main_env['use_fftw']:
    main_env['fftw_threads'] = 'none'

# Back to gcc if cuda is activated
if main_env['use_cuda'] and "g++" not in main_env['CXX']:
    raise StopError('GCC should be used when compiling with CUDA')

# Printing some build infos
if main_env['should_configure']:
    print_build_info(main_env)

# OpenMP flags - compiler dependent
omp_flags = {
    "g++": ["-fopenmp"],
    "clang++": ["-fopenmp"],
    "icpc": ["-qopenmp"]
}


def cxx_alias(cxx):
    for k in omp_flags.keys():
        if k in cxx:
            return k

    raise StopError('Unsupported compiler: ' + cxx)


cxx = cxx_alias(main_env['CXX'])

# Setting main compilation flags
main_env['CXXFLAGS'] = Split(main_env['CXXFLAGS'])
main_env['LINKFLAGS'] = main_env['CXXFLAGS']
main_env.AppendUnique(
    CXXFLAGS=Split('-std=c++14 -Wall -Wextra'),
    CPPDEFINES={
        'TAMAAS_LOOP_BACKEND': 'TAMAAS_LOOP_BACKEND_${backend.upper()}',
        'TAMAAS_FFTW_BACKEND': 'TAMAAS_FFTW_BACKEND_${fftw_threads.upper()}'
    },
)

if main_env['backend'] != 'cuda':
    main_env.AppendUnique(CXXFLAGS=['-pedantic'])

# Adding OpenMP flags
if main_env['backend'] == 'omp':
    main_env.AppendUnique(CXXFLAGS=omp_flags[cxx])
    main_env.AppendUnique(LINKFLAGS=omp_flags[cxx])
else:
    main_env.AppendUnique(CXXFLAGS=['-Wno-unknown-pragmas'])

# Correct bug in clang?
if main_env['backend'] == 'omp' and cxx == "clang++":
    main_env.AppendUnique(LIBS=["atomic"])
elif main_env['backend'] == 'tbb':
    main_env.AppendUnique(LIBS=['tbb'])

# Manage MPI compiler
if main_env['use_mpi']:
    main_env['CXX'] = '$MPICXX'
    main_env.AppendUnique(CPPDEFINES=['TAMAAS_USE_MPI'])
    main_env.AppendUnique(CXXFLAGS=['-Wno-cast-function-type'])

# Petsc defines
if main_env['use_petsc']:
    main_env.AppendUnique(CPPDEFINES=['TAMAAS_USE_PETSC'])

# Flags and options
if main_env['build_type'] == 'debug':
    main_env.AppendUnique(CPPDEFINES=['TAMAAS_DEBUG'])

# Define the scalar types
main_env.AppendUnique(CPPDEFINES={
    'TAMAAS_REAL_TYPE': main_env.subst('${real_type}'),
    'TAMAAS_INT_TYPE': main_env.subst('${integer_type}'),
})

# Compilation flags
cxxflags_dict = {
    "debug": Split("-g -O0"),
    "profiling": Split("-g -O3 -fno-omit-frame-pointer"),
    "release": Split("-O3")
}

if main_env['sanitizer'] != 'none':
    if main_env['backend'] == 'cuda':
        raise StopError("Sanitizers with cuda are not yet supported!")
    cxxflags_dict[build_type].append('-fsanitize=${sanitizer}')

main_env.AppendUnique(CXXFLAGS=cxxflags_dict[build_type])
main_env.AppendUnique(SHLINKFLAGS=cxxflags_dict[build_type])
main_env.AppendUnique(LINKFLAGS=cxxflags_dict[build_type])

if main_env['should_configure']:
    basic_checks(main_env)
    detect_dependencies(main_env)

# Writing information file
main_env.Tool('textfile')
main_env['SUBST_DICT'] = get_git_subst()

# Empty values if requested
if main_env['strip_info']:
    for k in main_env['SUBST_DICT']:
        main_env['SUBST_DICT'][k] = ""

# Substitution of environment file
main_env['SUBST_DICT'].update({
    '@build_type@': '$build_type',
    '@build_dir@': '${build_dir.abspath}',
    '@build_version@': '$version',
    '@backend@': '$backend',
})

# Environment file content
env_content = """export PYTHONPATH=@build_dir@/python:$$PYTHONPATH
export LD_LIBRARY_PATH=@build_dir@/src:$$LD_LIBRARY_PATH
"""

# Writing environment file
env_file = main_env.Textfile(
    main_env.File('tamaas_environment.sh', main_env['build_dir']), env_content)

# Default targets
build_targets = ['build-cpp', env_file]
install_targets = ['install-lib']

if main_env._get_major_minor_revision(scons_version)[0] >= 4:
    main_env.Tool('compilation_db')
    build_targets.append(
        main_env.CompilationDatabase(PRINT_CMD_LINE_FUNC=pretty_cmd_print
                                     if not main_env['verbose'] else None))

# Building Tamaas library
Export('main_env')
main_env.SubDirectory('src')

# Building Tamaas extra components
for dir in ['python', 'tests']:
    if main_env['build_{}'.format(dir)] and not main_env.GetOption('help'):
        main_env.SubDirectory(dir)
        build_targets.append('build-{}'.format(dir))

# Building API + Sphinx documentation if requested
main_env.SubDirectory('doc')
main_env.Alias('doc', 'build-doc')
install_targets.append('install-doc')

# Define dummy dev command when python is deactivated
if not main_env['build_python']:
    dummy_command(
        main_env, 'dev', 'Command "dev" does not do anything' +
        ' without python activated ("build_python=True")')
else:
    install_targets.append('install-python')

# Define dummy test command when tests are deactivated
if not main_env['build_tests']:
    dummy_command(
        main_env, 'test', 'Command "test" does not do anything' +
        ' without tests activated ("build_tests=True")')

# Definition of target aliases, a.k.a. sub-commands
main_env.Alias('build', build_targets)

# Define proper install targets
main_env.Alias('install', install_targets)

# Default target is to build stuff
main_env.Default('build')

# Building a tar archive
archive = main_env.Command(
    'tamaas-${version}.tar.gz',
    '',
    ('git archive '
     '--format=tar.gz '
     '--prefix=tamaas/ '
     '-o $TARGET HEAD'),
)
main_env.Alias('archive', archive)

linting(main_env)
