#include "kelvin.hh"
#include "model.hh"

#include <pybind11/pybind11.h>

namespace py = pybind11;

PYBIND11_MODULE(register_integral_operators, m) {
  m.def("register_kelvin_force", [](tamaas::Model& model) {
    model.registerIntegralOperator<
        tamaas::Kelvin<tamaas::model_type::volume_2d, 0>>("kelvin_force");
  });
  m.def("register_kelvin_disp", [](tamaas::Model& model) {
    model.registerIntegralOperator<
        tamaas::Kelvin<tamaas::model_type::volume_2d, 1>>("kelvin");
  });

  m.def("register_kelvin_grad", [](tamaas::Model& model) {
    model.registerIntegralOperator<
        tamaas::Kelvin<tamaas::model_type::volume_2d, 2>>("kelvin_gradient");
  });

  m.def("set_integration_method",
        [](tamaas::IntegralOperator& k, tamaas::integration_method method,
           tamaas::Real cutoff) {
          auto& k_ = dynamic_cast<tamaas::Kelvin<tamaas::model_type::volume_2d, 0>&>(k);
          k_.setIntegrationMethod(method, cutoff);
        });
}
