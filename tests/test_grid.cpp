/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "grid.hh"
#include "grid_view.hh"
#include "model_type.hh"
#include "test.hh"

using namespace tamaas;

/* -------------------------------------------------------------------------- */

// Testing 1D creation
TEST(TestGridCreation, Creation1d) {
  Grid<Real, 1> grid{{5}, 2};
  std::array<UInt, 1> correct_size{{5}};
  ASSERT_TRUE(grid.sizes() == correct_size) << "Wrong sizes";

  std::array<UInt, 2> correct_strides{{2, 1}};
  ASSERT_TRUE(grid.getStrides() == correct_strides) << "Wrong strides";
}

// Testing 2D creation
TEST(TestGridCreation, Creation2d) {
  Grid<Real, 2> grid{{5, 2}, 3};
  std::array<UInt, 2> correct_size{{5, 2}};
  ASSERT_TRUE(grid.sizes() == correct_size) << "Wrong sizes";

  std::array<UInt, 3> correct_strides{{6, 3, 1}};
  ASSERT_TRUE(grid.getStrides() == correct_strides) << "Wrong strides";
}

// Testing 3D creation
TEST(TestGridCreation, Creation3d) {
  Grid<Real, 3> grid{{8, 5, 2}, 3};
  std::array<UInt, 3> correct_size{8, 5, 2};
  ASSERT_TRUE(grid.sizes() == correct_size) << "Wrong sizes";

  std::array<UInt, 4> correct_strides{30, 6, 3, 1};
  ASSERT_TRUE(grid.getStrides() == correct_strides) << "Wrong strides";
}

/* -------------------------------------------------------------------------- */
TEST(TestWrap, Creation) {
  std::array<Real, 10> buffer;
  std::array<UInt, 2> shape{5, 2};
  span<Real> view{buffer.data(), 10};

  // Fill values
  std::iota(std::begin(buffer), std::end(buffer), 0);

  // Wrap grid on view
  Grid<Real, 2> grid(shape.begin(), shape.end(), 1, view);

  grid(1, 1) = 1;

  ASSERT_TRUE(compare(view, buffer));
  ASSERT_TRUE(compare(grid, buffer));
}

/* -------------------------------------------------------------------------- */

// Testing iterators in STL function iota
TEST(TestGridIterators, Iota) {
  constexpr UInt N = 20;
  Grid<UInt, 1> grid{{N}, 1};
  std::iota(grid.begin(), grid.end(), 0);

  const auto* p{grid.getInternalData()};
  for (UInt i = 0; i < N; ++i)
    ASSERT_TRUE(p[i] == i) << "Iota fill failed";
}

// Testing filling grid with OpenMP loop
TEST(TestGridIterators, OpenMPLoops) {
  Grid<UInt, 1> grid{{20}, 1};

#ifndef TAMAAS_USE_CUDA
#pragma omp parallel for
#endif
  for (auto it = grid.begin(); it < grid.end(); ++it) {
    UInt i = it - grid.begin();
    *it = i;
  }

  std::vector<UInt> correct(20);
  std::iota(correct.begin(), correct.end(), 0);

  ASSERT_TRUE(compare(grid, correct)) << "Fill using OpenMP loop failed";
}

/* -------------------------------------------------------------------------- */

// Testing access operator
TEST(TestGridAccess, 3D) {
  Grid<UInt, 3> grid{{1, 3, 5}, 3};
  std::iota(grid.begin(), grid.end(), 0);

  for (UInt i = 0; i < grid.sizes()[0]; ++i)
    for (UInt j = 0; j < grid.sizes()[1]; ++j)
      for (UInt k = 0; k < grid.sizes()[2]; ++k)
        for (UInt m = 0; m < grid.getNbComponents(); ++m)
          ASSERT_EQ(grid(i, j, k, m),
                    m + 3 * k + (3 * 5) * j + (3 * 5 * 3) * i);

  Grid<UInt, 2> grid1{{5, 3}, 1};
  grid1(2, 1, 0) = 1;
  ASSERT_EQ(grid1(2 * 3 + 1), 1);
}

/* -------------------------------------------------------------------------- */

// Testing scalar simple loop-based operators
TEST(TestGridOperators, LoopOperators) {
  Grid<UInt, 1> grid{{20}, 1};
  grid = 1;
  EXPECT_TRUE(std::all_of(grid.begin(), grid.end(), [](UInt x) {
    return x == 1;
  })) << "Assignement operator failed";
  grid += 1;
  EXPECT_TRUE(std::all_of(grid.begin(), grid.end(), [](UInt x) {
    return x == 2;
  })) << "Plus-equal operator failed";
}

// Testing loop-based functions with reductions
TEST(TestGridOperators, Stats) {
  constexpr UInt n = 20;
  const UInt N = mpi::allreduce<operation::plus>(n);

  Grid<Real, 1> grid{{n}, 1};
  std::iota(grid.begin(), grid.end(), 0);

  std::vector<Real> v(n);
  std::iota(v.begin(), v.end(), 0);

  const auto b = v.cbegin(), e = v.cend();
  Real min = mpi::allreduce<operation::min>(*std::min_element(b, e));
  Real max = mpi::allreduce<operation::max>(*std::max_element(b, e));
  Real sum = mpi::allreduce<operation::plus>(std::accumulate(b, e, 0.));
  Real mea = sum / N;

  std::transform(b, e, v.begin(),
                 [mea](Real v) { return (v - mea) * (v - mea); });
  Real acc = mpi::allreduce<operation::plus>(std::accumulate(b, e, 0.));
  Real var = acc / (N - 1);

  EXPECT_DOUBLE_EQ(grid.min(), min) << "Minimum function failed";
  EXPECT_DOUBLE_EQ(grid.max(), max) << "Maximum function failed";
  EXPECT_DOUBLE_EQ(grid.sum(), sum) << "Sum function failed";
  EXPECT_DOUBLE_EQ(grid.mean(), mea) << "Mean function failed";
  EXPECT_DOUBLE_EQ(grid.var(), var) << "Var function failed";
}

/* -------------------------------------------------------------------------- */

TEST(TestGridView, View2D) {
  Grid<UInt, 3> grid3d{{10, 10, 10}, 3};
  std::iota(grid3d.begin(), grid3d.end(), 0);

  for (UInt i : Loop::range(10)) {
    auto grid_view = make_view(grid3d, i);
    std::vector<UInt> reference(300);
    std::iota(reference.begin(), reference.end(), 300 * i);
    EXPECT_TRUE(
        std::equal(grid_view.begin(), grid_view.end(), reference.begin()))
        << "View on slice fail";
  }
}

TEST(TestGridView, ViewOnComponent) {
  Grid<UInt, 3> grid3d{{10, 9, 8}, 3}, solution{{10, 9, 8}, 3};
  grid3d = solution = 0;

  auto view = make_component_view(grid3d, 2u);
  std::iota(view.begin(), view.end(), 0);

  for (UInt i = 0; i < solution.getNbPoints(); ++i)
    solution(3 * i + 2) = i;

  EXPECT_TRUE(std::equal(grid3d.begin(), grid3d.end(), solution.begin()))
      << "View on components fail";

  view(9, 8, 7) = 1;
  ASSERT_EQ(grid3d(9, 8, 7, 2), 1);
}

TEST(TestGridView, ViewCopy) {
  Grid<UInt, 3> grid3d{{10, 10, 10}, 2};
  Grid<UInt, 1> copy, solution{{10}, 1};
  auto view_on_slice = make_view(grid3d, 3, 4);
  auto* view_on_slice_ptr = &view_on_slice;
  auto view = make_component_view(*view_on_slice_ptr, 1u);
  std::iota(view.begin(), view.end(), 0);
  std::iota(solution.begin(), solution.end(), 0);
  copy = view;
  EXPECT_TRUE(std::equal(copy.begin(), copy.end(), solution.begin()))
      << "Copy of view fail";
}

TEST(TestGridView, HigherOrderView) {
  Grid<UInt, 2> grid{{10, 10}, 2};
  std::iota(grid.begin(), grid.end(), 0);
  GridView<Grid, UInt, 2, 3> view{grid, {}, -1};

  EXPECT_TRUE(std::equal(grid.begin(), grid.end(), view.begin()))
      << "Higher order view fail";
}

/* -------------------------------------------------------------------------- */

struct BroadcastSet123 {
  CUDA_LAMBDA inline void operator()(VectorProxy<UInt, 3> v) {
    v(0) = 1;
    v(1) = 2;
    v(2) = 3;
  }
};

TEST(TestGridOperators, BroadcastOperators) {
  Grid<UInt, 2> grid{{10, 10}, 3}, solution{{10, 10}, 3};
  auto set = BroadcastSet123();

  // Filling up solution
  Loop::loop(set, range<VectorProxy<UInt, 3>>(solution));

  Vector<UInt, 3> v;
  v(0) = 1;
  v(1) = 2;
  v(2) = 3;

  grid = v;

  EXPECT_TRUE(std::equal(grid.begin(), grid.end(), solution.begin()))
      << "Broadcast assignement failure";

  v(1) = 1;
  v(2) = 1;

  solution += 1;
  grid += v;

  EXPECT_TRUE(std::equal(grid.begin(), grid.end(), solution.begin()))
      << "Broadcast += failure";

  solution -= 1;
  grid -= v;

  EXPECT_TRUE(std::equal(grid.begin(), grid.end(), solution.begin()))
      << "Broadcast -= failure";

  v(1) = 2;
  v(2) = 3;

  solution *= solution;
  grid *= v;

  EXPECT_TRUE(std::equal(grid.begin(), grid.end(), solution.begin()))
      << "Broadcast *= failure";

  Loop::loop(set, range<VectorProxy<UInt, 3>>(solution));
  grid /= v;

  EXPECT_TRUE(std::equal(grid.begin(), grid.end(), solution.begin()))
      << "Broadcast /= failure";
}

TEST(TestAllocation, HardType) {
  std::array<UInt, 2> dims{4, 5};
  auto grid = allocateGrid<model_type::basic_2d, true, Real>(dims, 1);
  static_assert(
      std::is_same<decltype(grid), std::unique_ptr<Grid<Real, 2>>>::value,
      "allocation type incorrect");
  EXPECT_TRUE(std::equal(dims.begin(), dims.end(), grid->sizes().begin()));
  EXPECT_EQ(grid->getNbComponents(), 1);
}

TEST(TestAllocation, SoftType) {
  std::array<UInt, 2> dims{4, 5};
  auto grid = allocateGrid<true, Real>(model_type::basic_2d, dims, 1);
  static_assert(
      std::is_same<decltype(grid), std::unique_ptr<GridBase<Real>>>::value,
      "allocation type incorrect");
  EXPECT_EQ(grid->dataSize(), 20);
  EXPECT_EQ(grid->getNbComponents(), 1);
}
