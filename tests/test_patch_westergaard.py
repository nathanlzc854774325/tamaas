# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division, print_function

from numpy.linalg import norm
import tamaas as tm


def test_patch_westergaard(patch_westergaard):
    model = patch_westergaard.model
    solution = patch_westergaard.solution
    pressure = patch_westergaard.pressure

    shape = (model.boundary_shape + [tm.type_traits[model.type].components])

    # Setting the correct pressure component
    traction = model.traction.reshape(shape)[..., -1]
    traction[:] = pressure

    # solveNeumann() doesn't do anything on volume models
    if model.type == tm.model_type.volume_2d:
        tm.ModelFactory.registerVolumeOperators(model)
        model.operators['boussinesq'](model.traction, model.displacement)
    else:
        model.solveNeumann()

    if model.type in [tm.model_type.volume_1d, tm.model_type.volume_2d]:
        output_displ = model.displacement[0, ..., -1]
    else:
        output_displ = model.displacement.reshape(shape)[..., -1]

    error = norm(solution - output_displ) / norm(solution)

    assert error < 1e-15 and norm(solution) > 1e-15, \
        "Neumann error = {}".format(error)

    # Removing model types where no dirichlet is defined yet
    if model.type in {tm.model_type.surface_1d,
                      tm.model_type.surface_2d,
                      tm.model_type.volume_2d}:
        return

    traction[:] = 0
    output_displ[:] = solution
    model.solveDirichlet()
    error = norm(pressure - traction) / norm(pressure)

    assert error < 1e-15, "Dirichlet error = {}".format(error)
