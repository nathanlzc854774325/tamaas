# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division

import tamaas as tm
import numpy as np


def createRandomSurface():
    """Return a surface from an object which loses a ref"""
    sg = tm.SurfaceGeneratorFilter2D([12, 12])
    sg.spectrum = tm.Isopowerlaw2D()
    sg.spectrum.q0 = 1
    sg.spectrum.q1 = 2
    sg.spectrum.q2 = 4
    sg.spectrum.hurst = 0.6

    return sg.buildSurface()


def registerField(model):
    """Register a field which loses a ref"""
    f = np.zeros([12, 12], dtype=tm.dtype)
    model['f'] = f


def test_surface_survival():
    surface = createRandomSurface()
    surface += 1
    assert surface.shape == (12, 12)


def test_register_field_survival():
    model = tm.ModelFactory.createModel(tm.model_type.basic_2d,
                                        [1., 1.],
                                        [12, 12])
    registerField(model)
    f = model['f']
    f += 1
    assert model['f'].shape == (12, 12)
