# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess
import os

from tamaas import TamaasInfo


def test_gtest(pytestconfig):
    me_path = os.path.realpath(__file__)
    current_dir = os.path.dirname(me_path)
    my_env = os.environ.copy()
    my_env['PYTHONPATH'] = current_dir + ":" + os.getenv('PYTHONPATH', "")

    args = [os.path.join(current_dir, 'test_gtest_all'),
            '--gtest_output=xml:' +
            os.path.join(current_dir, 'gtest_results.xml')]

    has_mpirun = True

    try:
        subprocess.check_call(['mpirun', '-V'], shell=True)
    except subprocess.CalledProcessError:
        has_mpirun = False

    subprocess.check_call(args, env=my_env)

    if not has_mpirun or not TamaasInfo.has_mpi:
        return

    for procs in range(2, 4):
        # Do not write output xml in parallel
        call_args = ['mpirun',
                     '--allow-run-as-root',
                     '--oversubscribe',
                     '-np', str(procs)] \
            + args[:1]
        subprocess.check_call(call_args, env=my_env)
