# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import tamaas as tm

from numpy.linalg import norm


def test_boussinesq_surface():
    # Definition of modeled domain
    model_type = tm.model_type.volume_2d
    discretization = [2, 128, 128]
    system_size = [1., 1., 1.]

    x, y = [
        np.linspace(0, size, n, endpoint=False, dtype=tm.dtype)
        for size, n in zip(system_size[1:], discretization[1:])
    ]
    xx, yy = np.meshgrid(x, y, sparse=True)
    tractions = np.zeros(discretization[1:] + [3], dtype=tm.dtype)

    for i in range(3):
        omega = np.random.randint(1, 20, (2, )) * 2 * np.pi
        tractions[:, :, i] = np.cos(omega[0] * xx) * np.cos(omega[1] * yy)

    # Material contants
    E = 1.  # Young's modulus
    nu = 0.3  # Poisson's ratio

    # Creation of model
    model_vol = tm.ModelFactory.createModel(model_type, system_size,
                                            discretization)
    model_surf = tm.ModelFactory.createModel(tm.model_type.surface_2d, [1, 1],
                                             tractions.shape[:-1])

    model_vol.E = model_surf.E = E
    model_vol.nu = model_surf.nu = nu

    # Setup for integral operators
    tm.ModelFactory.registerVolumeOperators(model_vol)

    # Pressure definition
    model_vol['traction'][...] = tractions
    model_surf['traction'][...] = tractions

    model_surf.solveNeumann()

    # Applying operator
    boussinesq = model_vol.operators["boussinesq"]
    boussinesq(model_vol['traction'], model_vol['displacement'])

    # # Dumper
    # dumper_helper = UVWDumper(residual, args.name)
    # model.addDumper(dumper_helper)
    # model.dump()
    # print("Done")

    for i in range(3):
        error = norm(model_vol.displacement[0, :, :, i]
                     - model_surf.displacement[:, :, i]) \
                     / norm(tractions[:, :, i])
        assert error < 1e-16
