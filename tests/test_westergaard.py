# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division, print_function

import tamaas as tm
import pytest
import numpy as np
from conftest import pkridfn

from numpy.linalg import norm


@pytest.fixture(scope="module",
                params=[tm.PolonskyKeerRey.pressure,
                        tm.PolonskyKeerRey.gap],
                ids=pkridfn)
def pkr(westergaard, request):
    loads = {
        tm.PolonskyKeerRey.pressure: westergaard.load,
        tm.PolonskyKeerRey.gap: 0.06697415181446396
    }

    model = tm.ModelFactory.createModel(tm.model_type.basic_1d, [1.],
                                        [westergaard.n])
    model.E, model.nu = westergaard.e_star, 0
    solver = tm.PolonskyKeerRey(model, westergaard.surface, 1e-10,
                                request.param,
                                request.param)
    solver.max_iter = 5000
    solver.solve(loads[request.param])
    return model, westergaard


@pytest.fixture(scope="module")
def kato(westergaard, request):
    load = westergaard.load

    model = tm.ModelFactory.createModel(tm.model_type.basic_1d, [1.],
                                        [westergaard.n])
    model.E, model.nu = westergaard.e_star, 0
    solver = tm.KatoSaturated(model, westergaard.surface, 1e-5, np.inf)
    solver.max_iter = 5000
    solver.solve(load)
    return model, westergaard


def test_energy_pkr(pkr):
    model, sol = pkr
    traction, displacement = model['traction'], model['displacement']
    error = norm((traction - sol.pressure)*(displacement - sol.displacement))
    error /= norm(sol.pressure * sol.displacement)
    assert error < 1e-10


def test_energy_kato(kato):
    model, sol = kato
    traction, displacement = model['traction'], model['displacement']
    error = norm((traction - sol.pressure)*(displacement - sol.displacement))
    error /= norm(sol.pressure * sol.displacement)
    assert error < 1e-5


@pytest.mark.xfail(tm.TamaasInfo.backend == 'tbb',
                   reason='TBB reductions are unstable?')
def test_pkr_multi(westergaard):
    model_basic = tm.ModelFactory.createModel(tm.model_type.basic_1d, [1.],
                                              [westergaard.n])
    model_volume = tm.ModelFactory.createModel(tm.model_type.volume_1d,
                                               [1., 1.],
                                               [20, westergaard.n])

    pressures = {}
    for model in [model_basic, model_volume]:
        print(model)
        solver = tm.PolonskyKeerRey(model, westergaard.surface, 1e-12,
                                    tm.PolonskyKeerRey.pressure,
                                    tm.PolonskyKeerRey.pressure)
        solver.solve(westergaard.load)
        pressures[model] = model['traction']

    error = norm(pressures[model_basic] - pressures[model_volume][:, 1])\
        / westergaard.load
    assert error < 1e-16


if __name__ == "__main__":
    print("Test is meant to run with pytest")
