# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division

import tamaas as tm
import numpy as np


def test_flood_fill2d():
    field = np.zeros((18, 18), bool)

    # Cluster of permeter 18 area 13
    field[2:4, 3:6] = True
    field[4, 4:6] = True  # noqa
    field[5, 5] = True  # noqa
    field[3:5, 6:8] = True  # noqa

    # Cluster of perimeter 8 area 4
    field[14:16, 3:5] = True

    # Cluster of perimeter 20 area 11
    field[9:11, 8:11] = True  # noqa
    field[7:9, 9] = True  # noqa
    field[10, 11:14] = True  # noqa

    # Cluster of perimeter 18 area 9
    field[3:5, 14:16] = True
    field[5:10, 15] = True

    # Cluster spanning periodic boundary
    field[0:3, 12:14] = True
    field[-2:, 12:14] = True

    # Percolating cluster
    field[12, :] = True

    if False:
        # Show contact map
        import matplotlib.pyplot as plt
        plt.imshow(field, interpolation='none', cmap='gray_r', origin='lower')
        plt.xlim(0, 17)
        plt.ylim(0, 17)
        plt.show()

    clusters = tm.FloodFill.getClusters(field, False)

    # Dummy class for clusters
    class dummy:

        def __init__(self, area, perimeter, extent=None):
            self.area = area
            self.perimeter = perimeter
            self.extent = extent

    # Solution
    ref = [
        dummy(10, 14, [5, 2]),   # cluster spanning pbc
        dummy(13, 18),
        dummy(9, 18),
        dummy(11, 20),
        dummy(18, 36, [1, 18]),  # percolating cluster
        dummy(4, 8)
    ]

    assert len(ref) == len(clusters)

    for x, y in zip(clusters, ref):
        assert x.area == y.area
        assert x.perimeter == y.perimeter

        if y.extent is not None:
            assert x.extent == y.extent

    field[:] = True
    field[2:4, 2:4] = False
    clusters = tm.FloodFill.getClusters(field, False)
    assert clusters[0].extent == [18, 18]


def test_flood_fill3d():
    field = np.zeros((5, 5, 5), bool)
    field[2:4, 3, 1:3] = True

    clusters = tm.FloodFill.getVolumes(field, False)

    assert len(clusters) == 1
    assert clusters[0].area == 4
