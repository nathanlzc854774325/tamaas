# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import tamaas as tm
import numpy as np


def test_voigt():
    shape = [3, 3, 3]
    stress = np.ones(shape + [9], dtype=tm.dtype)
    # voigt = np.zeros(shape + [6])
    voigt = tm.compute.to_voigt(stress)
    sol = np.ones(shape + [6], dtype=tm.dtype)
    sol[:, :, :, 3:] = np.sqrt(2)
    print(voigt)
    print(sol)
    assert np.all(np.abs(sol - voigt) < 1e-15)
