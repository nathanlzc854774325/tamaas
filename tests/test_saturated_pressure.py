# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import print_function, division

import numpy as np
import tamaas as tm
import pytest


@pytest.fixture(scope="module")
def saturated():
    grid_size = 256
    load = 0.06
    p_sat = 0.4

    x = np.linspace(0, 1, grid_size, endpoint=False)
    surface = np.sin(2 * np.pi * x)

    model = tm.ModelFactory.createModel(tm.model_type.basic_2d,
                                        [1., 1.], [grid_size, 1])
    model.E = 1.
    model.nu = 0

    solver = tm.KatoSaturated(model, surface, 1e-12, p_sat)
    solver.max_iter = 6000
    solver.solve(load)

    return model, p_sat, load


def test_mean_pressure(saturated):
    model, _, load = saturated
    assert abs(model.traction.mean() - load) / load < 1e-12


def test_max_pressure(saturated):
    model, p_sat, _ = saturated
    assert abs(model.traction.max() - p_sat) / p_sat < 1e-12


def test_orthogonality(saturated):
    model, _, _ = saturated
    error = np.max(model.traction * model['gap']) \
        / (model.traction.max() * model['gap'].max())
    assert error < 4e-8
