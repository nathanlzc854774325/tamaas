# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division

import os
import pytest
import numpy as np
import tamaas as tm

from pathlib import Path
from numpy.testing import assert_allclose
from tamaas.dumpers import NumpyDumper, JSONDumper, FieldDumper
from tamaas.dumpers._helper import directory_dump, step_dump


class Dumper(tm.ModelDumper):
    """Simple numpy dumper"""

    def __init__(self):
        tm.ModelDumper.__init__(self)

    def dump(self, model):
        np.savetxt('tractions.txt', np.ravel(model.traction))
        np.savetxt('displacement.txt', np.ravel(model.displacement))


dumper_types = [NumpyDumper, JSONDumper]

try:
    from tamaas.dumpers import H5Dumper
    dumper_types.append(H5Dumper)
except ImportError:
    pass

try:
    from tamaas.dumpers import UVWDumper
    dumper_types.append(UVWDumper)
except ImportError:
    pass

try:
    from tamaas.dumpers import NetCDFDumper
    dumper_types.append(NetCDFDumper)
except ImportError:
    pass


@pytest.mark.parametrize('dumper_class', dumper_types)
def test_dumper(model_fixture, tmp_path, dumper_class):
    os.chdir(tmp_path)
    filename = 'test_{}'.format(dumper_class.__name__)

    if not issubclass(dumper_class, FieldDumper):
        dumper = dumper_class(filename)
    else:
        dumper = dumper_class(filename, all_fields=True)
        filename = dumper.file_path

    dumper << model_fixture

    # UVW does not read VTK files
    try:
        if dumper_class is UVWDumper:
            with pytest.raises(NotImplementedError):
                dumper.read(filename)
            assert Path(filename).is_file()
            return
    except NameError:
        pass

    rmodel = dumper.read(filename)

    def compare(model, reference):
        assert model is not reference
        assert model.type == reference.type
        assert model.shape == reference.shape

        for field in reference:
            # assert field in model
            assert_allclose(model[field], reference[field], rtol=1e-15)

    compare(rmodel, model_fixture)

    # Skipping sequence read for JSON
    if dumper_class.__name__ == "JSONDumper":
        return

    dumper << model_fixture
    count = 0

    for rmodel in dumper.read_sequence(filename.replace("0000", "*")):
        count += 1
        compare(rmodel, model_fixture)

    assert count == 2


def test_custom_dumper(tmp_path):
    os.chdir(tmp_path)
    model = tm.ModelFactory.createModel(tm.model_type.volume_2d, [1., 1., 1.],
                                        [16, 4, 8])
    dumper = Dumper()
    dumper << model

    tractions = np.loadtxt('tractions.txt')
    displacements = np.loadtxt('displacement.txt')

    assert_allclose(tractions.reshape(model.traction.shape), model.traction,
                    1e-15)
    assert_allclose(displacements.reshape(model.displacement.shape),
                    model.displacement, 1e-15)


@pytest.fixture
def simple_model(scope="module"):
    return tm.ModelFactory.createModel(tm.model_type.basic_2d, [1, 1], [1, 1])


def test_directory_dump(simple_model, tmp_path):
    os.chdir(tmp_path)

    @directory_dump("dummy")
    class Dummy(tm.ModelDumper):
        def __init__(self):
            super(Dummy, self).__init__()

        def dump(self, model):
            pass

        @property
        def file_path(self):
            return 'dummy'

    dumper = Dummy()
    dumper << simple_model
    directory = Path('dummy')

    assert dumper.mkdir
    assert directory.is_dir()

    # Testing turning the behavior off
    directory.rmdir()
    dumper.mkdir = False
    dumper << simple_model
    assert not directory.is_dir()


def test_step_dump(simple_model, tmp_path):
    os.chdir(tmp_path)

    @step_dump
    class Dummy(FieldDumper):
        extension = 'dummy'

        def _dump_to_file(self, file_descriptor, model):
            with open(file_descriptor, 'w'):
                pass

    files = []
    dumper = Dummy('dummy')

    def dump():
        files.append(dumper.file_path)
        dumper << simple_model

    dump()
    dump()

    for file in files:
        assert Path(file).is_file()
