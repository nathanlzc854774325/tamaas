/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef POLONSKY_KEER_TAN_HH
#define POLONSKY_KEER_TAN_HH
/* -------------------------------------------------------------------------- */
#include "kato.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

class PolonskyKeerTan : public Kato {
public:
  /// Constructor
  PolonskyKeerTan(Model& model, const GridBase<Real>& surface, Real tolerance,
                  Real mu);

public:
  /// Solve with Coulomb friction
  Real solve(std::vector<Real> p0) override;
  /// Solve with Tresca friction
  Real solveTresca(GridBase<Real>& p0);

  /// Template for solve function
  template <model_type type>
  Real solveTmpl(GridBase<Real>& p0, bool use_tresca = false);
  /// Enforce pressure mean
  template <UInt comp>
  void enforcePressureMean(GridBase<Real>& p0);
  /// Compute mean of field (only on I_c)
  template <UInt comp>
  Vector<Real, comp> computeMean(GridBase<Real>& field, bool on_c);
  /// Compute squared norm
  Real computeSquaredNorm(GridBase<Real>& field);
  /// Restrict search direction on I_c
  template <UInt comp>
  void truncateSearchDirection(bool on_c);
  /// Compute optimal step size (only on I_c)
  template <UInt comp>
  Real computeStepSize(bool on_c);

private:
  std::unique_ptr<GridBase<Real>> search_direction = nullptr;
  std::unique_ptr<GridBase<Real>> search_direction_backup = nullptr;
  std::unique_ptr<GridBase<Real>> projected_search_direction = nullptr;
};

}  // namespace tamaas

#endif  // POLONSKY_KEER_TAN_HH
