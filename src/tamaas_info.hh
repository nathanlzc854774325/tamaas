/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef TAMAAS_INFO_HH
#define TAMAAS_INFO_HH
/* -------------------------------------------------------------------------- */
#include <string>
/* -------------------------------------------------------------------------- */

namespace tamaas {
struct TamaasInfo {
  static const std::string version;
  static const std::string build_type;
  static const std::string branch;
  static const std::string commit;
  static const std::string remotes;
  static const std::string diff;
  static const std::string backend;
  static const bool has_mpi;
  static const bool has_petsc;
};
}  // namespace tamaas

/* -------------------------------------------------------------------------- */
#endif  // TAMAAS_INFO_HH
