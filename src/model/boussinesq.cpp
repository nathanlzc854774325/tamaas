/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "boussinesq.hh"
#include "boussinesq_helper.hh"
#include "influence.hh"
#include "model.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

/// \cond DO_NOT_DOCUMENT
template <>
Boussinesq<model_type::volume_2d, 0>::Boussinesq(Model* model) : parent(model) {
  this->initialize(trait::dimension, trait::dimension);
}

template <>
Boussinesq<model_type::volume_2d, 1>::Boussinesq(Model* model) : parent(model) {
  this->initialize(trait::dimension, trait::voigt);
}
/// \endcond

/* -------------------------------------------------------------------------- */
/* Operator implementation */
/* -------------------------------------------------------------------------- */
template <model_type type, UInt derivative>
void Boussinesq<type, derivative>::apply(GridBase<Real>& source,
                                         GridBase<Real>& out) const {
  Real nu = this->model->getPoissonRatio(), mu = this->model->getShearModulus();
  influence::Boussinesq<trait::dimension, derivative> boussinesq(mu, nu);
  influence::ElasticHelper<trait::dimension> el(mu, nu);
  detail::BoussinesqHelper<type, decltype(boussinesq)> helper;

  // the source is a Grid<Real, bdim> grid normally, so we gotta make sure we
  // can cast it into a Grid<Real, dim>
  GridView<Grid, Real, trait::boundary_dimension, trait::dimension> view(
      source, {}, -1);

  parent::transformSource(view);

  helper.template apply<true>(this->source_buffer.front(), this->out_buffer,
                              this->wavevectors,
                              this->model->getSystemSize().front(), boussinesq);
  helper.makeFundamentalModeGreatAgain(this->source_buffer.front(),
                                       this->out_buffer, el);

  parent::transformOutput(
      [](auto&& out_buffer, auto layer) ->
      typename parent::BufferType& { return out_buffer[layer]; },
      out);
}

/* -------------------------------------------------------------------------- */
template <model_type type, UInt derivative>
void Boussinesq<type, derivative>::initialize(UInt source_components,
                                              UInt out_components) {
  auto hermitian_sizes =
      GridHermitian<Real, trait::boundary_dimension>::hermitianDimensions(
          this->model->getBoundaryDiscretization());

  // Initializing buffers
  this->source_buffer.resize(1);
  this->out_buffer.resize(this->model->getDiscretization()[0]);

  // Resizing source buffer
  std::for_each(this->source_buffer.begin(), this->source_buffer.end(),
                [&](auto& buffer) {
                  buffer.setNbComponents(source_components);
                  buffer.resize(hermitian_sizes);
                });

  // Resizing output buffer
  std::for_each(this->out_buffer.begin(), this->out_buffer.end(),
                [&](auto& buffer) {
                  buffer.setNbComponents(out_components);
                  buffer.resize(hermitian_sizes);
                });
}
}  // namespace tamaas
