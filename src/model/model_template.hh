/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef MODEL_TEMPLATE_HH
#define MODEL_TEMPLATE_HH
/* -------------------------------------------------------------------------- */
#include "grid_view.hh"
#include "model.hh"
#include "model_type.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

/**
 * @brief Model class templated with model type
 * Specializations of this class should take care of dimension specific code
 */
template <model_type type>
class ModelTemplate : public Model {
  using trait = model_type_traits<type>;
  static constexpr UInt dim = trait::dimension;
  using ViewType = GridView<Grid, Real, dim, trait::boundary_dimension>;

public:
  /// Constructor
  ModelTemplate(std::vector<Real> system_size,
                std::vector<UInt> discretization);

  // Get model type
  model_type getType() const override { return type; }

  std::vector<UInt> getGlobalDiscretization() const override;
  std::vector<UInt> getBoundaryDiscretization() const override;
  std::vector<Real> getBoundarySystemSize() const override;

  void setIntegrationMethod(integration_method method, Real cutoff) override;

protected:
  void initializeBEEngine();

protected:
  std::unique_ptr<ViewType> displacement_view = nullptr;
  std::unique_ptr<ViewType> traction_view = nullptr;
};

}  // namespace tamaas
/* -------------------------------------------------------------------------- */
#endif  // MODEL_TEMPLATE_HH
