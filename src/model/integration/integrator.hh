/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef INTEGRATOR_HH
#define INTEGRATOR_HH
/* -------------------------------------------------------------------------- */
#include "element.hh"
#include <expolit/expolit>
/* -------------------------------------------------------------------------- */
namespace tamaas {

#define BOUNDS                                                                 \
  thrust::pair<Real, Real> { -1, 1 }

template <UInt interpolation_order>
class Integrator {
  using element = ExponentialElement<interpolation_order>;
  static constexpr std::pair<Real, Real> bounds{-1, 1};

public:
  /// Standard integral of \f$\exp(\pm qy) \phi(y)\f$ over an element of radius
  /// \f$r\f$ and center \f$x_c\f$
  template <bool upper, UInt shape>
  __device__ __host__ static Real G0(Real q, Real r, Real xc) {
    const auto F = element::template g0<upper, shape>(q * r);
    return r * std::exp(element::sign(upper) * q * xc) *
           expolit::definite_integral(BOUNDS, F);
  }

  /// Standard integral of \f$qy\exp(\pm qy) \phi(y)\f$ over an element of
  /// radius \f$r\f$ and center \f$x_c\f$
  template <bool upper, UInt shape>
  __device__ __host__ static Real G1(Real q, Real r, Real xc) {
    const auto c = q * r;
    const auto integrals =
        std::make_pair(element::template g0<upper, shape>(c),
                       element::template g1<upper, shape>(c));
    return r * std::exp(element::sign(upper) * q * xc) *
           (q * xc * expolit::definite_integral(BOUNDS, integrals.first) +
            expolit::definite_integral(BOUNDS, integrals.second));
  }

  /// Standard integral of \f$\phi(y)\f$ over an element of radius
  /// \f$r\f$ and center \f$x_c\f$. Used for fundamental mode
  template <UInt shape>
  __device__ __host__ constexpr static Real F(Real r) {
    return r * expolit::definite_integral(BOUNDS,
                                          element::template shapes<shape>());
  }
};

// static member declare
template <UInt interpolation_order>
constexpr std::pair<Real, Real> Integrator<interpolation_order>::bounds;
}  // namespace tamaas

#undef BOUNDS

#endif  // INTEGRATOR_HH
