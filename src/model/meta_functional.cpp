/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "meta_functional.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

namespace functional {

Real MetaFunctional::computeF(GridBase<Real>& variable,
                              GridBase<Real>& dual) const {
  Real F = 0;
  for (const auto& f : functionals)
    F += f->computeF(variable, dual);
  return F;
}

/* -------------------------------------------------------------------------- */

void MetaFunctional::computeGradF(GridBase<Real>& variable,
                                  GridBase<Real>& gradient) const {
  gradient = 0;
  for (const auto& f : functionals)
    f->computeGradF(variable, gradient);
}

/* -------------------------------------------------------------------------- */

void MetaFunctional::addFunctionalTerm(std::shared_ptr<Functional> functional) {
  functionals.push_back(std::move(functional));
}

/* -------------------------------------------------------------------------- */
void MetaFunctional::clear() { functionals.clear(); }

}  // namespace functional

}  // namespace tamaas
/* -------------------------------------------------------------------------- */
