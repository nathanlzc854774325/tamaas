/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "volume_potential.hh"
#include "model.hh"
#include <algorithm>
/* -------------------------------------------------------------------------- */
namespace tamaas {

template <model_type type>
VolumePotential<type>::VolumePotential(Model* model)
    : IntegralOperator(model), wavevectors(), source_buffer(), out_buffer(),
      engine(FFTEngine::makeEngine()) {
  // Copy horizontal sizes
  std::array<UInt, trait::boundary_dimension> sizes;
  std::copy_n(model->getDiscretization().begin() + 1, trait::boundary_dimension,
              sizes.begin());

  auto hermitian_sizes =
      GridHermitian<Real, trait::boundary_dimension>::hermitianDimensions(
          sizes);

  wavevectors =
      FFTEngine::template computeFrequencies<Real, trait::boundary_dimension,
                                             true>(hermitian_sizes);

  // Normalize wavevectors
  auto system_size = model->getBoundarySystemSize();
  VectorProxy<const Real, trait::boundary_dimension> boundary_domain{
      system_size[0]};
  wavevectors *= 2 * M_PI;
  wavevectors /= boundary_domain;
  wavevectors *= -1.;  // < this is important for the convolution computation
}

/* -------------------------------------------------------------------------- */
template <model_type type>
model_type VolumePotential<type>::getType() const {
  return this->model->getType();
}

/* -------------------------------------------------------------------------- */
template <model_type type>
void VolumePotential<type>::initialize(UInt source_components,
                                       UInt out_components,
                                       UInt out_buffer_size) {
  auto hermitian_sizes =
      GridHermitian<Real, trait::boundary_dimension>::hermitianDimensions(
          this->model->getBoundaryDiscretization());

  // Initializing buffers
  this->source_buffer.resize(this->model->getDiscretization()[0]);
  this->out_buffer.resize(out_buffer_size);

  // Resizing source buffer
  std::for_each(this->source_buffer.begin(), this->source_buffer.end(),
                [&](auto& buffer) {
                  buffer.setNbComponents(source_components);
                  buffer.resize(hermitian_sizes);
                });

  // Resizing output buffer
  std::for_each(this->out_buffer.begin(), this->out_buffer.end(),
                [&](auto& buffer) {
                  buffer.setNbComponents(out_components);
                  buffer.resize(hermitian_sizes);
                });
}

/* -------------------------------------------------------------------------- */
template <model_type type>
void VolumePotential<type>::transformSource(GridBase<Real>& in,
                                            filter_t pred) const {
  constexpr UInt dim = trait::dimension;
  auto& i = dynamic_cast<Grid<Real, dim>&>(in);

  // Transforming source
  for (UInt layer : Loop::range(i.sizes().front())) {
    if (not pred(layer))
      continue;

    auto in_layer = make_view(i, layer);
    engine->forward(in_layer, source_buffer[layer]);
  }

  for (auto&& layer : out_buffer)
    layer = 0;
}

/* -------------------------------------------------------------------------- */
/* Template instanciation */
/* -------------------------------------------------------------------------- */
template class VolumePotential<model_type::volume_1d>;
template class VolumePotential<model_type::volume_2d>;

}  // namespace tamaas
