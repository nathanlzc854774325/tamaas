/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "adhesion_functional.hh"
/* -------------------------------------------------------------------------- */

namespace tamaas {

namespace functional {

Real ExponentialAdhesionFunctional::computeF(
    GridBase<Real>& gap, GridBase<Real>& /*pressure*/) const {
  auto rho_inv = 1. / getParameters().at("rho");
  auto gamma = getParameters().at("surface_energy");

  return -gamma * Loop::reduce<operation::plus>(
                      [rho_inv] CUDA_LAMBDA(const Real& g) {
                        return std::exp(-g * rho_inv);
                      },
                      gap);
}

void ExponentialAdhesionFunctional::computeGradF(
    GridBase<Real>& gap, GridBase<Real>& gradient) const {
  auto rho_inv = 1. / getParameters().at("rho");
  auto gamma = getParameters().at("surface_energy");

  Loop::loop(
      [rho_inv, gamma] CUDA_LAMBDA(const Real& g, Real& grad) {
        grad += gamma * std::exp(-g * rho_inv) * rho_inv;
      },
      gap, gradient);
}

Real MaugisAdhesionFunctional::computeF(GridBase<Real>& gap,
                                        GridBase<Real>& /*pressure*/) const {
  auto rho = getParameters().at("rho");
  auto rho_inv = 1. / rho;
  auto gamma = getParameters().at("surface_energy");

  return -gamma * Loop::reduce<operation::plus>(
                      [rho, rho_inv] CUDA_LAMBDA(const Real& g) {
                        return (g > rho) ? 0 : 1 - g * rho_inv;
                      },
                      gap);
}

void MaugisAdhesionFunctional::computeGradF(GridBase<Real>& gap,
                                            GridBase<Real>& gradient) const {
  auto rho = getParameters().at("rho");
  auto rho_inv = 1. / rho;
  auto gamma = getParameters().at("surface_energy");

  Loop::loop(
      [rho, rho_inv, gamma] CUDA_LAMBDA(const Real& g, Real& grad) {
        grad += (g > rho) ? 0 : gamma * rho_inv;
      },
      gap, gradient);
}

Real SquaredExponentialAdhesionFunctional::computeF(
    GridBase<Real>& gap, GridBase<Real>& /*pressure*/) const {
  auto rho_inv = 1. / getParameters().at("rho");
  auto rho_inv_2 = rho_inv * rho_inv;
  auto gamma = getParameters().at("surface_energy");

  return -gamma * Loop::reduce<operation::plus>(
                      [rho_inv_2] CUDA_LAMBDA(const Real& g) {
                        return std::exp(-0.5 * g * g * rho_inv_2);
                      },
                      gap);
}

void SquaredExponentialAdhesionFunctional::computeGradF(
    GridBase<Real>& gap, GridBase<Real>& gradient) const {
  auto rho_inv = 1. / getParameters().at("rho");
  auto rho_inv_2 = rho_inv * rho_inv;
  auto gamma = getParameters().at("surface_energy");

  Loop::loop(
      [rho_inv_2, gamma] CUDA_LAMBDA(const Real& g, Real& grad) {
        grad += g * rho_inv_2 * gamma * std::exp(-0.5 * g * g * rho_inv_2);
      },
      gap, gradient);
}

}  // namespace functional

}  // namespace tamaas
