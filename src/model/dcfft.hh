/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef DCFFT_HH
#define DCFFT_HH
/* -------------------------------------------------------------------------- */
#include "westergaard.hh"
/* -------------------------------------------------------------------------- */

namespace tamaas {

/**
 * @brief Non-periodic Boussinesq operator, computed with padded FFT
 */
class DCFFT
    : public Westergaard<model_type::basic_2d, IntegralOperator::neumann> {
  using trait = model_type_traits<model_type::basic_2d>;
  static constexpr UInt dim = trait::dimension;
  static constexpr UInt bdim = trait::boundary_dimension;
  static constexpr UInt comp = trait::components;

public:
  DCFFT(Model* model);

  /// Apply influence coefficients in Fourier domain
  void apply(GridBase<Real>& input, GridBase<Real>& output) const override;

private:
  /// Init influence with real-space square patch solution
  void initInfluence();

private:
  mutable Grid<Real, bdim> extended_buffer;
};

}  // namespace tamaas

#endif  // DCFFT_HH
