/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef RESIDUAL_HH
#define RESIDUAL_HH
/* -------------------------------------------------------------------------- */
#include "boussinesq.hh"
#include "materials/material.hh"
#include "mindlin.hh"
#include "model_type.hh"
/* -------------------------------------------------------------------------- */
#include <unordered_set>
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

/**
 * @brief Residual manager
 */
class Residual {
  static constexpr auto type = model_type::volume_2d;
  static constexpr auto dim = model_type_traits<type>::dimension;
  static constexpr auto voigt = model_type_traits<type>::voigt;

public:
  /// Constructor
  Residual(Model& model, std::shared_ptr<Material> material);
  /// Destructor
  virtual ~Residual() = default;

public:
  /// Compute the residual vector for a given strain increment
  virtual void computeResidual(GridBase<Real>& strain_increment);
  /// Compute residual surface displacement
  virtual void computeResidualDisplacement(GridBase<Real>& strain_increment);
  /// Apply tangent to arbitrary increment
  virtual void applyTangent(GridBase<Real>& output, GridBase<Real>& input,
                            GridBase<Real>& strain_increment);
  /// Update the plastic state
  virtual void updateState(GridBase<Real>& converged_strain_increment);
  /// Return model reference
  const Model& getModel() const { return model; }
  /// Return model reference (non-const)
  Model& getModel() { return model; }
  /// Return residual vector
  const GridBase<Real>& getVector() const { return *residual; }
  /// Return material
  const Material& getMaterial() const { return *material; }

private:
  /// Convenience function
  const IntegralOperator& integralOperator(const std::string& name) const {
    return *model.getIntegralOperator(name);
  }

  /// Add non-zero layers of plastic strain into the filter
  void updateFilter(Grid<Real, dim>& plastic_strain_increment);

protected:
  Model& model;
  std::shared_ptr<Material> material;
  std::shared_ptr<Grid<Real, 3>> strain, stress, residual, tmp;
  std::unordered_set<UInt> plastic_layers;
  std::function<bool(UInt)> plastic_filter;
};

}  // namespace tamaas
#endif
