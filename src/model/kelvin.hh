/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef KELVIN_HH
#define KELVIN_HH
/* -------------------------------------------------------------------------- */
#include "grid_hermitian.hh"
#include "influence.hh"
#include "integration/accumulator.hh"
#include "kelvin_helper.hh"
#include "model_type.hh"
#include "volume_potential.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

/// Integration method enumeration
enum class integration_method { cutoff, linear };

/**
 * @brief Kelvin tensor
 */
template <model_type type, UInt derivative>
class Kelvin : public VolumePotential<type> {
  static_assert(type == model_type::volume_1d || type == model_type::volume_2d,
                "Only volume types are supported");
  using trait = model_type_traits<type>;
  using dtrait = derivative_traits<derivative>;
  using parent = VolumePotential<type>;

protected:
  using KelvinInfluence = influence::Kelvin<trait::dimension, derivative>;
  using ktrait = detail::KelvinTrait<KelvinInfluence>;
  using Source = typename ktrait::source_t;
  using Out = typename ktrait::out_t;
  using filter_t = typename parent::filter_t;

public:
  /// Constructor
  Kelvin(Model* model);

  /// Apply the Kelvin-tensor_order operator
  void applyIf(GridBase<Real>& source, GridBase<Real>& out,
               filter_t pred) const override;

  /// Set the integration method for volume operator
  void setIntegrationMethod(integration_method method, Real cutoff);

  std::pair<UInt, UInt> matvecShape() const override;
  GridBase<Real> matvec(GridBase<Real>& X) const override;

private:
  void linearIntegral(GridBase<Real>& out, KelvinInfluence& kelvin) const;
  void cutoffIntegral(GridBase<Real>& out, KelvinInfluence& kelvin) const;

protected:
  integration_method method = integration_method::linear;
  Real cutoff;
};

}  // namespace tamaas

#endif  // KELVIN_HH
