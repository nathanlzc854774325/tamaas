/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef INTEGRAL_OPERATOR_HH
#define INTEGRAL_OPERATOR_HH
/* -------------------------------------------------------------------------- */
#include "field_container.hh"
#include "grid_base.hh"
#include "model_type.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

class Model;

/**
 * @brief Generic class for integral operators
 */
class IntegralOperator : public FieldContainer {
public:
  /// Kind of operator
  enum kind { neumann, dirichlet, dirac };

public:
  /// Constructor
  IntegralOperator(Model* model) : model(model) {}

  /// Apply operator on input
  virtual void apply(GridBase<Real>& input, GridBase<Real>& output) const = 0;

  /// Apply operator on filtered input
  virtual void applyIf(GridBase<Real>& input, GridBase<Real>& output,
                       const std::function<bool(UInt)>& /*unused*/) const {
    apply(input, output);
  }

  /// Update any data dependent on model parameters
  virtual void updateFromModel() {}

  /// Get model
  const Model& getModel() const { return *model; }

  /// Kind
  virtual kind getKind() const { return dirac; }

  /// Type
  virtual model_type getType() const;

  /// Norm
  virtual Real getOperatorNorm() {
    throw not_implemented_error{TAMAAS_MSG("operator does not implement norm")};
  }

  /// Dense shape (for Scipy integration)
  virtual std::pair<UInt, UInt> matvecShape() const {
    throw not_implemented_error{
        TAMAAS_MSG("operator does not implement "
                   "scipy.sparse.linalg.LinearOperator interface")};
  }

  /// matvec definition
  virtual GridBase<Real> matvec(GridBase<Real>& /*x*/) const {
    throw not_implemented_error{
        TAMAAS_MSG("operator does not implement "
                   "scipy.sparse.linalg.LinearOperator interface")};
  }

protected:
  Model* model = nullptr;
};

/* -------------------------------------------------------------------------- */
/* Printing IntegralOperator::kind to string */
/* -------------------------------------------------------------------------- */
std::ostream& operator<<(std::ostream& o, const IntegralOperator::kind& val);
}  // namespace tamaas

#endif  // INTEGRAL_OPERATOR_HH
