/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef FUNCTIONAL_HH
#define FUNCTIONAL_HH
/* -------------------------------------------------------------------------- */
#include "be_engine.hh"
#include "tamaas.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {

namespace functional {

/// Generic functional class for the cost function of the optimization problem
class Functional {
public:
  /// Destructor
  virtual ~Functional() = default;

public:
  /// Compute functional
  virtual Real computeF(GridBase<Real>& variable,
                        GridBase<Real>& dual) const = 0;
  /// Compute functional gradient
  virtual void computeGradF(GridBase<Real>& variable,
                            GridBase<Real>& gradient) const = 0;
};

}  // namespace functional

}  // namespace tamaas
/* -------------------------------------------------------------------------- */
#endif  // _FUNCTIONAL_HH
