# -*- mode:python; coding: utf-8 -*-
# vim: set ft=python:
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from SCons.Script import Import, Export


def prepend(env, path, list):
    return [env.File(x, path) for x in list]


Import('main_env')
env = main_env.Clone()
env.AddMethod(prepend, 'PrependDir')

# Core
core_list = """
fft_engine.cpp
grid.cpp
grid_hermitian.cpp
statistics.cpp
tamaas.cpp
loop.cpp
computes.cpp
logger.cpp
mpi_interface.cpp
""".split()
core_list = env.PrependDir('core', core_list)

if env['use_fftw']:
    core_list += ['core/fftw/fftw_engine.cpp']

    if env['use_mpi']:
        core_list += ['core/fftw/mpi/fftw_mpi_engine.cpp']

if env['use_cuda']:
    core_list += ['core/cuda/cufft_engine.cpp']

info_file = env.Substfile('tamaas_info.cpp', 'tamaas_info.cpp.in')
env.Depends(info_file, Value(env.subst(env['SUBST_DICT'].values())))
core_list.append(info_file)

# Lib roughcontact
generator_list = """
surface_generator.cpp
surface_generator_filter.cpp
surface_generator_random_phase.cpp
isopowerlaw.cpp
regularized_powerlaw.cpp
""".split()
generator_list = env.PrependDir('surface', generator_list)

# Lib PERCOLATION
percolation_list = """
flood_fill.cpp
""".split()
percolation_list = env.PrependDir('percolation', percolation_list)

# Model
model_list = """
model.cpp
model_factory.cpp
model_type.cpp
model_template.cpp
integral_operator.cpp
be_engine.cpp
westergaard.cpp
dcfft.cpp
elastic_functional.cpp
meta_functional.cpp
adhesion_functional.cpp
volume_potential.cpp
kelvin.cpp
mindlin.cpp
boussinesq.cpp
hooke.cpp
residual.cpp

materials/linear_elastic.cpp
materials/isotropic_hardening.cpp

integration/element.cpp
""".split()
model_list = env.PrependDir('model', model_list)

# Solvers
solvers_list = """
contact_solver.cpp
polonsky_keer_rey.cpp
kato_saturated.cpp
kato.cpp
beck_teboulle.cpp
condat.cpp
polonsky_keer_tan.cpp
ep_solver.cpp
dfsane_solver.cpp
epic.cpp
anderson.cpp
""".split()

if env['use_petsc']:
    solvers_list += ['petsc_solver.cpp']
solvers_list = env.PrependDir('solvers', solvers_list)

# Assembling total list
rough_contact_list = \
  core_list + generator_list + percolation_list + model_list + solvers_list

# Adding extra warnings for Tamaas base lib
env.AppendUnique(CXXFLAGS=['-Wextra'])

# Allowing libTamaas.so to find libs in the same directory
env.AppendUnique(RPATH=["'$$$$ORIGIN'"])

# Build static library for packaging
if env['build_static_lib']:
    env.AppendUnique(CXXFLAGS='-fPIC')
    libTamaas = env.StaticLibrary('Tamaas', rough_contact_list)
# Build shared library (default)
else:
    libTamaas = env.SharedLibrary('Tamaas', rough_contact_list,
                                  SHLIBVERSION=env['version'])


# Specify install target to install lib
lib_prefix = env.Dir('lib', env['prefix'])
lib_install = env.InstallVersionedLib(target=lib_prefix,
                                      source=libTamaas)

# Defining alias targets
main_env.Alias('build-cpp', libTamaas)
main_env.Alias('install-lib', lib_install)

# Export target for use in python builds
Export('libTamaas')
