/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */

#include "tamaas.hh"
#include "errors.hh"
#include "fftw/interface.hh"
#include "logger.hh"
#include "mpi_interface.hh"
#if TAMAAS_LOOP_BACKEND == TAMAAS_LOOP_BACKEND_OMP
#include <omp.h>
#endif
#ifdef TAMAAS_USE_PETSC
#include <petscsys.h>
#endif

/* -------------------------------------------------------------------------- */

namespace tamaas {

void initialize(UInt num_threads) {
  static bool has_warned = false;

  mpi::thread provided = mpi::thread::single;

  if (not mpi::initialized()) {
    mpi::init_thread(nullptr, nullptr, mpi::thread::multiple, &provided);
  }

  bool should_init_threads = (provided > mpi::thread::single);

#if TAMAAS_LOOP_BACKEND == TAMAAS_LOOP_BACKEND_OMP
  if (num_threads)
    omp_set_num_threads(num_threads);  // set user-defined number of threads
  else
    num_threads = omp_get_max_threads();
#else
  if (num_threads != 0)
    num_threads = 1;
#endif

  Logger().get(LogLevel::debug) << "requested " << num_threads << " threads\n";

#if TAMAAS_FFTW_BACKEND != TAMAAS_FFTW_BACKEND_NONE
  if (should_init_threads and (not fftw::init_threads())) {
    throw std::runtime_error(TAMAAS_MSG("FFTW could not initialize threads!"));
  } else if (not should_init_threads)
    Logger().get(LogLevel::debug) << "not initializing FFTW threads\n";
#endif

  if (mpi::initialized()) {
    if (not has_warned) {
      Logger().get(LogLevel::warning) << "experimental MPI support\n";
      has_warned = true;
    }
    fftw::mpi::init();
  }

  if (should_init_threads) {
#if TAMAAS_FFTW_BACKEND != TAMAAS_FFTW_BACKEND_NONE
    Logger().get(LogLevel::debug)
        << "initializing FFTW with " << num_threads << " threads\n";
    fftw::plan_with_nthreads(num_threads);
#endif
  }

#ifdef TAMAAS_USE_CUDA
  Logger().get(LogLevel::warning) << "experimental Cuda support\n";
#endif

#ifdef TAMAAS_USE_PETSC
  PetscCallAbort(mpi::comm::world(), PetscInitialize(nullptr, nullptr, "", ""));
#endif
}

/* -------------------------------------------------------------------------- */

void finalize() {
  if (not mpi::finalized()) {
#if TAMAAS_BACKEND != TAMAAS_BACKEND_CPP
    fftw::cleanup_threads();
#endif
    fftw::mpi::cleanup();

    mpi::finalize();
  }
}

namespace {
/// Manager for initialize + finalize
struct entry_exit_points {
  entry_exit_points() { initialize(); }
  ~entry_exit_points() { finalize(); }
  static const entry_exit_points singleton;
};

const entry_exit_points entry_exit_points::singleton;
}  // namespace

}  // namespace tamaas
