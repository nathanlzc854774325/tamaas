/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef LOOP_UTILS_HH
#define LOOP_UTILS_HH
/* -------------------------------------------------------------------------- */
#include "errors.hh"
#include "tamaas.hh"
#include <limits>
#include <thrust/functional.h>
#include <type_traits>
/* -------------------------------------------------------------------------- */

namespace tamaas {

namespace detail {

template <bool only_points = false, typename... Grids>
UInt loopSize(Grids&&... grids) {
  return (only_points)
             ? std::get<0>(std::forward_as_tuple(grids...)).getNbPoints()
             : std::get<0>(std::forward_as_tuple(grids...)).dataSize();
}

template <typename... Sizes>
void areAllEqual(bool /*unused*/ /*unused*/,
                 std::ptrdiff_t /*unused*/ /*unused*/) {}

template <typename... Sizes>
void areAllEqual(bool result, std::ptrdiff_t prev, std::ptrdiff_t current) {
  if (!(result && prev == current))
    throw std::length_error(TAMAAS_MSG("ranges do not have the same size"));
}

template <typename... Sizes>
void areAllEqual(bool result, std::ptrdiff_t prev, std::ptrdiff_t current,
                 Sizes... rest) {
  areAllEqual(result && prev == current, current, rest...);
}
}  // namespace detail

template <typename... Ranges>
void checkLoopSize(Ranges&&... ranges) {
  detail::areAllEqual(true, (ranges.end() - ranges.begin())...);
}

namespace detail {
template <operation op, typename ReturnType>
struct reduction_helper;

template <typename ReturnType>
struct reduction_helper<operation::plus, ReturnType>
    : public thrust::plus<ReturnType> {
  ReturnType init() const { return ReturnType(0); }
};

template <typename ReturnType>
struct reduction_helper<operation::times, ReturnType>
    : public thrust::multiplies<ReturnType> {
  ReturnType init() const { return ReturnType(1); }
};

template <typename ReturnType>
struct reduction_helper<operation::min, ReturnType>
    : public thrust::minimum<ReturnType> {
  ReturnType init() const { return std::numeric_limits<ReturnType>::max(); }
};

template <typename ReturnType>
struct reduction_helper<operation::max, ReturnType>
    : public thrust::maximum<ReturnType> {
  ReturnType init() const { return std::numeric_limits<ReturnType>::lowest(); }
};

}  // namespace detail

}  // namespace tamaas

#endif  // __LOOP_UTILS_HH
