/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef APPLY_HH
#define APPLY_HH
/* -------------------------------------------------------------------------- */
#include "tamaas.hh"
#include <cstddef>
#include <thrust/tuple.h>
#include <utility>
/* -------------------------------------------------------------------------- */

namespace tamaas {

namespace detail {

/// Helper function for application of a functor on a thrust::tuple
template <size_t nargs>
struct Apply {
  template <typename Functor, typename Tuple, typename... Args>
  __host__ __device__ static auto apply(Functor&& func, Tuple&& t,
                                        Args&&... args)
      -> decltype(Apply<nargs - 1>::apply(std::forward<Functor>(func),
                                          std::forward<Tuple>(t),
                                          thrust::get<nargs - 1>(t),
                                          std::forward<Args>(args)...)) {
    return Apply<nargs - 1>::apply(
        std::forward<Functor>(func), std::forward<Tuple>(t),
        thrust::get<nargs - 1>(t), std::forward<Args>(args)...);
  }
};

template <>
struct Apply<0> {
  template <typename Functor, typename Tuple, typename... Args>
  __host__ __device__ static auto apply(Functor&& func, Tuple&& /*t*/,
                                        Args&&... args)
      -> decltype(func(std::forward<Args>(args)...)) {
    return func(std::forward<Args>(args)...);
  }
};

/// Helper class for functor application in thrust
template <typename Functor, typename ret_type = void>
class ApplyFunctor {

public:
  __host__ __device__ ApplyFunctor(const Functor& functor) : functor(functor) {}
  __host__ __device__ ApplyFunctor(const ApplyFunctor& o)
      : functor(o.functor) {}

  template <typename Tuple>
  __host__ __device__ ret_type operator()(Tuple&& t) const {
    return Apply<thrust::tuple_size<typename std::remove_reference<
        Tuple>::type>::value>::apply(functor, std::forward<Tuple>(t));
  }

private:
  const Functor& functor;
};

}  // namespace detail

}  // namespace tamaas

#endif
