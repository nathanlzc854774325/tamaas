/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef FFTW_MPI_INTERFACE_HH
#define FFTW_MPI_INTERFACE_HH
/* -------------------------------------------------------------------------- */
#include "mpi_interface.hh"
#include <cstdlib>
#include <numeric>
#include <stdexcept>
#include <tuple>
#ifdef TAMAAS_USE_MPI
#include <fftw3-mpi.h>
#endif
/* -------------------------------------------------------------------------- */
namespace fftw_impl {
namespace mpi_dummy {
inline void init() {}
inline void cleanup() {}
inline auto local_size_many(int rank, const std::ptrdiff_t* size,
                            std::ptrdiff_t howmany) {
  return std::make_tuple(howmany * std::accumulate(size, size + rank,
                                                   std::ptrdiff_t{1},
                                                   std::multiplies<void>()),
                         size[0], 0);
}
}  // namespace mpi_dummy

#ifdef TAMAAS_USE_MPI
namespace mpi_impl {
inline void init() { fftw_mpi_init(); }
inline void cleanup() { fftw_mpi_cleanup(); }
inline auto local_size_many(int rank, const std::ptrdiff_t* size,
                            std::ptrdiff_t howmany) {
  if (rank < 2)
    throw std::domain_error("FFTW-MPI cannot be used for 1D transforms");

  std::ptrdiff_t local_n0, local_n0_offset;
  auto res = fftw_mpi_local_size_many(
      rank, size, howmany, FFTW_MPI_DEFAULT_BLOCK, ::tamaas::mpi::comm::world(),
      &local_n0, &local_n0_offset);
  return std::make_tuple(res, local_n0, local_n0_offset);
}
}  // namespace mpi_impl
namespace mpi = mpi_impl;
#else
namespace mpi = mpi_dummy;
#endif
}  // namespace fftw_impl
#endif
