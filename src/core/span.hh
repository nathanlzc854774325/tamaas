/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef SPAN_HH
#define SPAN_HH
/* -------------------------------------------------------------------------- */
#include "tamaas.hh"
#include <cstddef>
#include <iterator>
#include <type_traits>
/* -------------------------------------------------------------------------- */
namespace tamaas {

template <typename T>
struct span {
  using element_type = T;
  using value_type = std::remove_cv_t<T>;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using pointer = T*;
  using const_pointer = const T*;
  using reference = T&;
  using const_reference = const T&;
  using iterator = T*;
  using reverse_iterator = std::reverse_iterator<iterator>;

  constexpr size_type size() const noexcept { return size_; }
  constexpr pointer data() const noexcept { return data_; }

  constexpr iterator begin() const noexcept { return data_; }
  constexpr iterator end() const noexcept { return data_ + size_; }
  constexpr iterator begin() noexcept { return data_; }
  constexpr iterator end() noexcept { return data_ + size_; }

  reference operator[](size_type idx) { return data_[idx]; }

  const_reference operator[](size_type idx) const { return data_[idx]; }

  pointer data_ = nullptr;
  size_type size_ = 0;
};

}  // namespace tamaas

/* -------------------------------------------------------------------------- */
#endif
