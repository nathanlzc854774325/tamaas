/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef ERRORS_HH
#define ERRORS_HH
/* -------------------------------------------------------------------------- */
#include <sstream>
#include <stdexcept>
/* -------------------------------------------------------------------------- */
namespace tamaas {

class assertion_error : public std::invalid_argument {
  using std::invalid_argument::invalid_argument;
};

class not_implemented_error : public std::runtime_error {
  using std::runtime_error::runtime_error;
};

class model_type_error : public std::domain_error {
  using std::domain_error::domain_error;
};

class nan_error : public std::runtime_error {
  using std::runtime_error::runtime_error;
};

struct no_convergence_error : public std::runtime_error {
  no_convergence_error(const std::string& what, double error, double tolerance)
      : std::runtime_error(what), error(error), tolerance(tolerance) {}

  const char* what() const noexcept override {
    std::stringstream sstr;
    std::string msg{std::runtime_error::what()};
    sstr << msg << " did not converge (" << std::scientific << error << " > "
         << tolerance << ")";
    return sstr.str().c_str();
  }

  double error, tolerance;
};

namespace detail {

template <class T>
void append_to_stream(std::ostream& s, T&& head) {
  s << head;
}

template <class T, class... Args>
void append_to_stream(std::ostream& s, T&& head, Args&&... tail) {
  append_to_stream(s, head);
  append_to_stream(s, tail...);
}

template <class... Args>
std::string concat_args(Args&&... args) {
  std::stringstream sstr;
  // C++17 (sstr << ... << args) and remove below;
  append_to_stream(sstr, args...);
  return sstr.str();
}

// Macros
#define TAMAAS_LOCATION __FILE__, ':', __LINE__, ':', __func__, "(): "

#define TAMAAS_MSG(...)                                                        \
  ::tamaas::detail::concat_args(TAMAAS_LOCATION, __VA_ARGS__)

#define TAMAAS_ASSERT(cond, ...)                                               \
  do {                                                                         \
    if (not(cond))                                                             \
      throw assertion_error(TAMAAS_MSG(__VA_ARGS__));                          \
  } while (0)

}  // namespace detail

}  // namespace tamaas

#endif  // ERRORS_HH
