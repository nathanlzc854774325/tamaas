/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "fft_engine.hh"

#if TAMAAS_USE_CUDA
#include "cuda/cufft_engine.hh"
#else
#include "fftw/fftw_engine.hh"
#ifdef TAMAAS_USE_MPI
#include "fftw/mpi/fftw_mpi_engine.hh"
#include "mpi_interface.hh"
#endif
#endif

/* -------------------------------------------------------------------------- */
namespace tamaas {

std::unique_ptr<FFTEngine> FFTEngine::makeEngine(unsigned int flags) {
#define inst(x)                                                                \
  do {                                                                         \
    Logger().get(LogLevel::debug) << TAMAAS_MSG("[" #x "] Init");              \
    return std::make_unique<x>(flags);                                         \
  } while (0)

#ifdef TAMAAS_USE_MPI
  if (mpi::size() != 1)
    inst(FFTWMPIEngine);
  else
    inst(FFTWEngine);
#elif TAMAAS_USE_CUDA
  inst(CuFFTEngine);
#else
  inst(FFTWEngine);
#endif

#undef inst
}

template <>
std::vector<std::array<UInt, 2>>
FFTEngine::realCoefficients<2>(const std::array<UInt, 2>& local_sizes) {
  constexpr UInt dim = 2;
  std::vector<std::array<UInt, dim>> indices;
  indices.reserve(dim * dim);

  auto n = Partitioner<dim>::global_size(local_sizes);
  std::array<bool, 2> even{{n[0] % 2 == 0, n[1] % 2 == 0}};

  indices.push_back({{0, 0}});

  if (even[0])
    indices.push_back({{n[0] / 2, 0}});

  if (even[1])
    indices.push_back({{0, n[1] / 2}});

  if (even[0] and even[1])
    indices.push_back({{n[0] / 2, n[1] / 2}});

  return indices;
}

template <>
std::vector<std::array<UInt, 1>>
FFTEngine::realCoefficients<1>(const std::array<UInt, 1>& local_sizes) {
  constexpr UInt dim = 1;
  std::vector<std::array<UInt, dim>> indices;
  indices.push_back({{0}});

  if (local_sizes[0] % 2 == 0)
    indices.push_back({{local_sizes[0] / 2}});

  return indices;
}

}  // namespace tamaas
