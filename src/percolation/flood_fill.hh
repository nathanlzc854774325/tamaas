/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#ifndef FLOOD_FILL_H
#define FLOOD_FILL_H
/* -------------------------------------------------------------------------- */
#include "grid.hh"
#include <list>
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

template <UInt dim>
class Cluster {
  using Point = std::array<Int, dim>;
  using BBox = std::pair<std::array<Int, dim>, std::array<Int, dim>>;

public:
  /// Constructor
  Cluster(Point start, const Grid<bool, dim>& map, Grid<bool, dim>& visited,
          bool diagonal);
  /// Copy constructor
  Cluster(const Cluster& other);
  /// Default constructor
  Cluster() = default;
  /// Get area of cluster
  UInt getArea() const { return getPoints().size(); }
  /// Get perimeter of cluster
  UInt getPerimeter() const { return perimeter; }
  /// Get contact points
  const auto& getPoints() const { return points; }
  /// Get bounding box
  BBox boundingBox() const;
  /// Get bounding box extent
  std::array<Int, dim> extent() const;
  /// Assign next neighbors
  auto getNextNeighbors(const std::array<Int, dim>& p);
  /// Assign diagonal neighbors
  auto getDiagonalNeighbors(const std::array<Int, dim>& p);

private:
  /// List of points in the cluster
  std::vector<Point> points;
  /// Perimeter size (number of segments)
  UInt perimeter = 0;
};

/* -------------------------------------------------------------------------- */

class FloodFill {
  template <UInt dim>
  using List = std::vector<Cluster<dim>>;

public:
  /// Return a list of connected segments
  static List<1> getSegments(const Grid<bool, 1>& map);
  /// Return a list of connected areas
  static List<2> getClusters(const Grid<bool, 2>& map, bool diagonal);
  /// Return a list of connected volumes
  static List<3> getVolumes(const Grid<bool, 3>& map, bool diagonal);
};

/* -------------------------------------------------------------------------- */
}  // namespace tamaas
/* -------------------------------------------------------------------------- */
#endif  // FLOOD_FILL_H
