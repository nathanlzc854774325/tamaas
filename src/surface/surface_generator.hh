/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

/* -------------------------------------------------------------------------- */
#ifndef SURFACE_GENERATOR_HH
#define SURFACE_GENERATOR_HH
/* -------------------------------------------------------------------------- */
#include "grid.hh"
#include <array>
/* -------------------------------------------------------------------------- */

namespace tamaas {

/// Class generating random surfaces
template <UInt dim>
class SurfaceGenerator {
public:
  /// Default constructor
  SurfaceGenerator() = default;
  /// Construct with surface global size
  SurfaceGenerator(std::array<UInt, dim> global_size) {
    setSizes(std::move(global_size));
  }

  /// Default destructor
  virtual ~SurfaceGenerator() = default;

public:
  /// Build surface profile (array of heights)
  virtual Grid<Real, dim>& buildSurface() = 0;
  /// Set surface sizes
  void setSizes(std::array<UInt, dim> n);
  /// Set surface sizes
  void setSizes(const UInt n[dim]);
  /// Get surface sizes
  auto getSizes() const { return global_size; }

  long getRandomSeed() const { return random_seed; }
  void setRandomSeed(long seed);

protected:
  Grid<Real, dim> grid;
  std::array<UInt, dim> global_size{0};
  long random_seed = 0;
};

/* -------------------------------------------------------------------------- */

}  // namespace tamaas

#endif  // SURFACE_GENERATOR_HH
