/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */

#ifndef SURFACE_GENERATOR_RANDOM_PHASE_H
#define SURFACE_GENERATOR_RANDOM_PHASE_H
/* -------------------------------------------------------------------------- */
#include "filter.hh"
#include "surface_generator_filter.hh"
#include "tamaas.hh"
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

template <UInt dim>
class SurfaceGeneratorRandomPhase : public SurfaceGeneratorFilter<dim> {
public:
  using SurfaceGeneratorFilter<dim>::SurfaceGeneratorFilter;
  /// Build surface with uniform random phase
  Grid<Real, dim>& buildSurface() override;
};

}  // namespace tamaas

#endif
