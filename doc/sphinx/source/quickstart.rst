Quickstart
----------

Here is a quick introduction to get you started with Tamaas.

.. _pip-install:

Installation from PyPI
^^^^^^^^^^^^^^^^^^^^^^

If you have a Linux system, or have installed the `Windows Subsystem for Linux
<https://ubuntu.com/tutorials/install-ubuntu-on-wsl2-on-windows-10#1-overview>`_,
you can simply run ``python3 -m pip install tamaas``. This installs the latest
stable version of Tamaas from `PyPI <https://pypi.org/project/tamaas/>`_. You
can get the latest cutting-edge build of Tamaas with:

.. code-block:: bash

  python3 -m pip install \
    --extra-index-url https://gitlab.com/api/v4/projects/19913787/packages/pypi/simple \
    tamaas


.. note:: To limit the number of statically linked dependencies in the wheel
   package, the builds that can be installed via PyPI or the GitLab package
   registery do not include parallelism or architecture specific optimizations.
   If you want to execute Tamaas in parallel, or want to improve performance, it
   is highly recommanded that you install with Spack or compile from source with
   the instructions below.

Installation with Spack
^^^^^^^^^^^^^^^^^^^^^^^

If you have `Spack <https://spack.io/>`_ installed (e.g. in an HPC environment),
you can install Tamaas with the following:

.. code-block:: bash

  spack install tamaas

This will install an MPI-enabled version of tamaas with Scipy solvers. The
command ``spack info tamaas`` shows the build variants. To disable MPI, you
should disable MPI in the FFTW variant:

.. code-block:: bash

  spack install tamaas ^fftw~mpi

The Spack package for Tamaas allows installation from the repository with
``spack install tamaas@master``.

You can also use Spack to create container recipes with ``spack containerize``.
Here's an example ``spack.yaml`` for Tamaas which creates an image with OpenMPI
and NetCDF so that Tamaas can be executed in parallel within the container with
`Singularity <https://sylabs.io/docs/>`_:

.. code-block:: yaml

    spack:
    config:
        build_jobs: 2
    specs:
    - matrix:
        - [py-mpi4py, py-netcdf4, tamaas@master+solvers]
        - ["^openmpi@4 fabrics=ofi,psm2,ucx schedulers=none"]
    concretizer:
        unify: true
    container:
        format: singularity
        images:
        os: ubuntu:20.04
        spack: develop
        os_packages:
        final:
            - gfortran

.. _docker-image:

Docker image
^^^^^^^^^^^^

We provide Docker images that are automatically built and pused to the Gitlab
registry for compatibility reasons (mainly with macOS). To get the latest image
simply run:

.. code-block:: bash

  docker pull registry.gitlab.com/tamaas/tamaas
  # to rename for convenience
  docker tag registry.gitlab.com/tamaas/tamaas tamaas

Then you can run scripts directly:

.. code-block:: bash

  docker run -v $PWD:/app -t tamaas python3 /app/your_script.py
  # or
  docker run tamaas tamaas surface --sizes 16 16 --cutoffs 4 4 8 --hurst 0.8 | docker run -i tamaas tamaas contact 0.1 > results.npy


.. warning:: The provided Docker image and Dockerfile offer limited
             customization options and are hard-wired to use OpenMPI. If the use
             case/goal is to run in HPC environements, containers generated with
             Spack should be preferred, as they allow greater flexibility in the
             dependency selection and installed packages.


The ``Dockerfile`` at the root of the Tamaas repository can be used to build an
image containing Tamaas with a full set of dependencies and customize the build
options. Simply run:

.. code-block:: bash

  docker build -t tamaas .

.. tip::
   The following arguments can be passed to docker to customize the Tamaas
   build (with the ``--build-arg`` flag for ``docker build``):

   - ``BACKEND``: parallelism model for loops
   - ``FFTW_THREADS``: parallelism model for FFTW3
   - ``USE_MPI``: compile an MPI-parallel version of Tamaas

   See below for explanations.


Singularity container
^^^^^^^^^^^^^^^^^^^^^

A `singularity`_ container can be created from the docker image with:

.. code-block:: bash

  singularity build tamaas.sif docker://registry.gitlab.com/tamaas/tamaas

To run the image with MPI:

.. code-block:: bash

  mpirun singularity exec --home=$PWD tamaas.sif python3 <your_script>


Installation from source
^^^^^^^^^^^^^^^^^^^^^^^^

First make sure the following dependencies are installed for Tamaas:

- a **C++ compiler** with full **C++14** and **OpenMP** support
- **SCons** (python build system)
- **FFTW3**
- **thrust** (1.9.2+)
- **boost** (pre-processor)
- **python 3+** with **numpy**
- **pybind11** (included as submodule)
- **expolit** (included as submodule)

Optional dependencies are:

- an MPI implementation
- **FFTW3** with MPI/threads/OpenMP (your pick) support
- **scipy** (for nonlinear solvers)
- **uvw**, **h5py**, **netCDF4** (for dumpers)
- **googletest** and **pytest** (for tests)
- **Doxygen** and **Sphinx** (for documentation)

.. tip:: On a HPC environment, use the following variables to specify where the
   dependencies are located:

   - ``FFTW_ROOT``
   - ``THRUST_ROOT``
   - ``BOOST_ROOT``
   - ``PYBIND11_ROOT``
   - ``GTEST_ROOT``

.. note:: You can use the provided Dockerfile to build an image with the
   external dependencies installed.

You should first clone the git repository with the submodules that are
dependencies to tamaas (`pybind11 <https://github.com/pybind/pybind11>`_ and
`googletest <https://github.com/google/googletest>`_)::

    git clone --recursive https://gitlab.com/tamaas/tamaas.git

The build system uses `SCons <https://scons.org/>`_. In order to compile Tamaas
with the default options::

    scons

After compiling a first time, you can edit the compilation options in the file
``build-setup.conf``, or alternatively supply the options directly in the
command line:

.. code-block:: bash

    scons option=value [...]

To get a list of **all** build options and their possible values, you can run
``scons -h``. You can run ``scons -H`` to see the SCons-specific options (among
them ``-j n`` executes the build with ``n`` threads and ``-c`` cleans the
build). Note that the build is aware of the ``CXX`` and ``CXXFLAGS`` environment
variables.

Once compiled, you can install the python module with:

.. code-block:: bash

  scons install prefix=/your/prefix

The above command automatically calls ``pip`` if it is installed. If want to
install an editable package for development, the best practice is to run the
following in a `virtual environment
<https://docs.python.org/3/library/venv.html>`_:

.. code-block:: bash

  scons dev

This is equivalent to running ``pip install -e build-release/python[all]``. You
can check that everything is working fine with:

.. code-block:: bash

  python3 -c 'import tamaas; print(tamaas)'


Important build options
.......................

Here are a selected few important compilation options:

``build_type``
    Controls the type of build, which essentially changes the optimisation level
    (``-O0`` for ``debug``, ``-O2`` for ``profiling`` and ``-O3`` for
    ``release``) and the amount of debug information. Default type is
    ``release``. Accepted values are:

    - ``release``
    - ``debug``
    - ``profiling``

``CXX``
    Compiler (uses the environment variable ``CXX`` by default).

``CXXFLAGS``
    Compiler flags (uses the environment variable ``CXXFLAGS`` by
    default). Useful to add tuning flags (e.g. ``-march=native -mtune=native``
    for GCC or ``-xHOST`` for Intel), or additional optimisation flags (e.g.
    ``-flto`` for link-time optimization).

``backend``
    Controls the Thrust parallelism backend. Defaults to ``omp`` for OpenMP.
    Accepted values are:

    - ``omp``: OpenMP
    - ``cpp``: Sequential
    - ``tbb`` (unsupported): Threading Building Blocks

``fftw_threads``
    Controls the FFTW thread model. Defaults to ``omp`` for OpenMP. Accepted
    values are:

    - ``omp``: OpenMP
    - ``threads``: PThreads
    - ``none``: Sequential

``use_mpi``
    Activates MPI-parallelism (boolean value).

More details on the above options can be found in :doc:`performance`.

Building the docs
^^^^^^^^^^^^^^^^^

Documentation is built using ``scons doc``. Make sure to have the correct
dependencies installed (they are already provided in the Docker image).

Running the contact pipe tools
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once installed, the python package provides a `tamaas` command, which defines
three subcommands that can be used to explore the mechanics of elastic rough
contact:

- ``surface`` generates randomly rough surfaces (see :doc:`rough_surfaces`)
- ``contact`` solves an elastic contact problem with a surface read from
  ``stdin`` (see :doc:`contact`)
- ``plot`` plots the surface tractions and displacements read from
  ``stdin``

Here's a sample command line for you to try out:

.. code-block:: bash

  tamaas surface --cutoffs 2 4 64 --size 512 512 --hurst 0.8 | tamaas contact 1 | tamaas plot

Check out the help of each command (e.g. ``tamaas surface -h``) for a description
of the arguments.

Running the tests
^^^^^^^^^^^^^^^^^

You need to activate the ``build_tests`` option to compile the tests:

.. code-block:: bash

  scons build_tests=True

Tests can then be run with the ``scons test`` command. Make sure you have
`pytest <https://docs.pytest.org/en/latest/>`_ installed.
