Developer documentation
=======================

If you have code contributions you'd like to submit to Tamaas, you are in the
right place. This page describes the good practices and workflows to follow to
get your code in Tamaas.

Documentation
-------------

Documentation can be built with ``scons doc``, provided the adequate
dependencies are installed. The generated files are in
``build-${build_type}/doc/``, with the C++ API in the ``doxygen/html`` subfolder
and the Sphinx documentation in the ``sphinx/html`` subfolder. Use them as
reference when developing, most functions and classes in Tamaas have API-level
documentation.

Writing documentation
~~~~~~~~~~~~~~~~~~~~~

Every new functionality in Tamaas should have *at minimum* API-level
documentation. In C++, this is done with `Doxygen-styled comments
<https://www.doxygen.nl/manual/docblocks.html>`_.

API-level documentation in Python can be done with `pybind11's docstrings
<https://pybind11.readthedocs.io/en/stable/advanced/misc.html#generating-documentation-using-sphinx>`_.


Writing code
------------

To check that your code matches Tamaas' coding convention, please run ``scons
lint`` *before* committing. This will check the code style of your changes. It
runs ``clang-format``, ``clang-tidy``, ``flake8`` and ``mypy``.

.. tip::
   Subcommands can be used to run individual linting software:

   - ``scons clang-format``
   - ``scons clang-tidy``
   - ``scons flake8``
   - ``scons mypy``

.. note::
   As redundancy measure, linting also occurs in continuous integration on
   Gitlab. Review the step artifacts to see which changes are necessary.


Running tests
-------------

.. warning::
   Make sure to run in a virtual environment where Tamaas is installed with
   ``scons dev``, see the :ref:`faq`.

To run tests on your local machine::

  scons build_tests=True
  scons test

The ``verbose=True`` build option ensures that test output is verbose as well,
useful when writing new parametrized tests. Individual tests can be run with::

  pytest build-release/tests/test_westergaard.py

Check the output for the lines indicating where the Tamaas python extension and
the ``libTamaas`` shared libraries were loaded from. For instance, an editable
install of Tamaas should give an output like::

  module file: <path-to-tamaas>/build-release/python/tamaas/__init__.py
  wrapper: <path-to-tamaas>/build-release/python/tamaas/_tamaas.cpython-311-x86_64-linux-gnu.so
  libTamaas: static link


Git workflow
------------

Please follow the very simple `GitHub flow
<https://docs.github.com/en/get-started/using-github/github-flow#following-github-flow>`_
to submit changes, essentially:

1. Fork the project and create a branch to contain your changes
2. Make your changes
3. Create a merge request
4. Address review changes

Commit messages
~~~~~~~~~~~~~~~

Commit messages are a very important trace of changes to the code, and should
contain enough context information to understand the motivation for the change.
A typical commit message structure should be:

.. code-block:: text 

  one-line short description of the change

  1 or more paragraphs detailing, for example:
  - what problem the commit fixes
  - anything non-trivial the code does
  - things that had to be researched to fix the issue
  - other important contextual information

There is no limit to a commit message length, and that's a good thing: the more
context the better, particularly for new functionality and non-trivial changes
(like breaking changes).
