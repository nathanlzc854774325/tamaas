#!/usr/bin/env python3
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import os

import numpy as np
import tamaas as tm

from tamaas.dumpers import H5Dumper as Dumper
from tamaas.dumpers._helper import hdf5toVTK
from tamaas.utils import publications

parser = argparse.ArgumentParser(
    description="Hertzian tractios applied on elastic half-space")
parser.add_argument("radius", type=float, help="Radius of sphere")
parser.add_argument("load", type=float, help="Applied normal force")
parser.add_argument("name", help="Output file name")
parser.add_argument("--plots", help='Show surface normal stress',
                    action="store_true")

args = parser.parse_args()

# Definition of modeled domain
model_type = tm.model_type.volume_2d
discretization = [32, 127, 127]
system_size = [0.25, 1., 1.]

# Material contants
E = 1.                # Young's modulus
nu = 0.3              # Poisson's ratio
E_star = E/(1-nu**2)  # Hertz modulus

# Creation of model
model = tm.ModelFactory.createModel(model_type,
                                    system_size,
                                    discretization)

model.E = E
model.nu = nu

# Setup for integral operators
tm.ModelFactory.registerVolumeOperators(model)

# Coordinates
x = np.linspace(0, system_size[1], discretization[1], endpoint=False)
y = np.linspace(0, system_size[2], discretization[2], endpoint=False)
x, y = np.meshgrid(x, y, indexing='ij')

center = [0.5, 0.5]
r = np.sqrt((x-center[0])**2 + (y-center[1])**2)

# Span of local data
local_size = model.boundary_shape
local_offset = tm.mpi.local_offset(r.shape)
local_slice = np.s_[local_offset:local_offset+local_size[0], :]

# Sphere
R = args.radius
P = args.load

# Contact area
a = (3*P*R/(4*E_star))**(1/3)
p_0 = 3 * P / (2 * np.pi * a**2)

# Pressure definition
traction = model.traction
hertz_traction = np.zeros(discretization[1:])
hertz_traction[r < a] = p_0 * np.sqrt(1 - (r[r < a] / a)**2)
traction[..., 2] = hertz_traction[local_slice]

# Array references
displacement = model.displacement
stress = model['stress']
gradient = np.zeros_like(stress)

# Getting integral operators
boussinesq = model.operators["boussinesq"]
boussinesq_gradient = model.operators["boussinesq_gradient"]

# Applying operators
boussinesq(traction, displacement)
boussinesq_gradient(traction, gradient)
model.operators["hooke"](gradient, stress)

# Dumper
dumper_helper = Dumper(args.name, 'stress')
model.addDumper(dumper_helper)
model.dump()

# Converting HDF dump to VTK
with tm.mpi.sequential():
    if tm.mpi.rank() == 0:
        hdf5toVTK(os.path.join('hdf5', f'{args.name}_0000.h5'), args.name)

if args.plots:
    import matplotlib.pyplot as plt  # noqa
    fig, ax = plt.subplots(1, 2)
    fig.suptitle("Rank {}".format(tm.mpi.rank()))
    ax[0].set_title("Traction")
    ax[1].set_title("Normal Stress")

    ax[0].imshow(traction[..., 2])
    ax[1].imshow(stress[0, ..., 2])
    fig.tight_layout()
    plt.show()

publications()
