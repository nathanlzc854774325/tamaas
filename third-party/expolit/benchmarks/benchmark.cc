/**
 * @file benchmark.cc
 * @author Lucas Frérot
 * @date Thu Mar 3 2016
 */

#include <expolit/expolit>
#include <benchmark/benchmark.h>
#include <random>

using namespace expolit;

constexpr Polynomial<Real, 1> z({0, 1});
using lin = std::remove_cv_t<decltype(z)>;

static void compact_integral(benchmark::State& state) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1, 2);
  Real q = dis(gen);
  std::pair<Real, Real> bounds(-1, 1);
  for (auto _ : state) {
    auto integrant = exp(q * z) * ((1 + z) * 0.5);
    auto value = definite_integral(bounds, integrant);
    benchmark::DoNotOptimize(value);
  }
}

static void constexpr_integral(benchmark::State& state) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1, 2);
  Real q = dis(gen);
  constexpr auto expo = integrate(exp(z));
  constexpr auto xexpo = integrate(z * exp(z));
  auto factor = 1 / q;
  for (auto _ : state) {
    std::pair<Real, Real> bounds(-q, q);
    auto value = 0.5 * (expo(bounds.second) - expo(bounds.first)) +
      0.5 * factor * (xexpo(bounds.second) - xexpo(bounds.first));
    benchmark::DoNotOptimize(value);
  }
}

using Q = Litteral<struct Q_>;

static void substitution_integral(benchmark::State& state) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> dis(1, 2);
  Real q = dis(gen);
  constexpr Q qv;

  constexpr auto integral = integrate(exp(qv * z) * ((1 + z) * 0.5));
  for (auto _ : state) {
    auto f = substitute<Q::tag>(Constant<Real>({q}), integral);
    auto value = f(1) - f(-1);
    benchmark::DoNotOptimize(value);
  }
}


BENCHMARK(compact_integral);
BENCHMARK(constexpr_integral);
BENCHMARK(substitution_integral);

BENCHMARK_MAIN();
