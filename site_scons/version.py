# -*- coding: utf-8 -*-
#
# Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
# Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
# Copyright (©) 2020-2024 Lucas Frérot
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess
import base64
from zlib import compress


def git(*args):
    """Run a git command"""
    return bytes(subprocess.check_output(["git"] + list(args),
                                         stderr=subprocess.STDOUT))


def get_git_subst():
    """Get info about state of git repository"""
    try:
        branch = git("rev-parse", "--abbrev-ref", "HEAD")[:-1]
        commit = git("rev-parse", branch)[:-1]
        diff = git("diff", "HEAD")
        remotes = git('remote', '-v')
    except subprocess.CalledProcessError as err:
        if b'not a git repository' in err.output:
            print("Not in a git repository: skipping info gathering")
        branch, commit, diff, remotes = [bytes()] * 4

    if remotes != b"":
        remotes_string = remotes[:-1].decode().replace('\n', '\\\\n')
    else:
        remotes_string = '""'

    return {
        '@commit@': commit.decode(),
        '@branch@': branch.decode(),
        '@diff@': base64.b64encode(compress(diff, 9)).decode(),
        '@remotes@': remotes_string,
    }
