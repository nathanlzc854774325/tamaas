# Tamaas - A high-performance library for periodic rough surface contact

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3479236.svg)](https://doi.org/10.5281/zenodo.3479236)
[![status](https://joss.theoj.org/papers/86903c51f3c66964eef7776d8aeaf17d/status.svg)](https://joss.theoj.org/papers/86903c51f3c66964eef7776d8aeaf17d)
[![Documentation Status](https://readthedocs.org/projects/tamaas/badge/?version=latest)](https://tamaas.readthedocs.io/en/latest/?badge=latest)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/tamaas%2Ftutorials/HEAD)

Tamaas (from تماس meaning “contact” in Arabic and Farsi) is a high-performance
rough-surface periodic contact code based on boundary and volume integral
equations. The clever mathematical formulation of the underlying numerical
methods allows the use of the fast-Fourier Transform, a great help in achieving
peak performance: Tamaas is consistently two orders of magnitude faster (and
lighter) than traditional FEM! Tamaas is aimed at researchers and practitioners
wishing to compute realistic contact solutions for the study of interface
phenomena.

## Disclaimer

This package is intended for ease of installation on x86\_64 Linux platforms
(and Windows Subsystem for Linux), but comes with NO WARRANTY of compatibility
(although it is manylinux2014\_x86\_64 compliant). If you experience any issue,
please install Tamaas from [source](https://gitlab.com/tamaas/tamaas) or with
[Spack](https://spack.io). We provide a Docker image for non-Linux systems
(should be suitable for macOS). Note that the version of Tamaas provided by this
package is not parallel.

This version of Tamaas is statically linked to [FFTW3](http://fftw.org/)
developed by Matteo Frigo and Steven G. Johnson, released under the terms of the
GNU Public License ([source code](https://github.com/FFTW/fftw3)).

Tamaas is the result of a science research project. To give proper credit to
Tamaas and the researchers who have developed the numerical methods that it
implements, please cite the [JOSS
paper](https://joss.theoj.org/papers/86903c51f3c66964eef7776d8aeaf17d), we also
provide the function `tamaas.utils.publications` which lists appropriate
citations when called at the end of a script.

## Dependencies

Essentials:

- Numpy

Optional:

- Scipy (for non-linear solvers)
- UVW (for dumpers)
- h5py (for dumpers)
- netCDF4 (for dumpers)

To install with all dependencies, run ``pip install tamaas[all]``.

## Documentation

Documentation can be found on
[tamaas.readthedocs.io](https://tamaas.readthedocs.io/en/latest/).

## Changelog

The changelog can be consulted
[here](https://gitlab.com/tamaas/tamaas/-/blob/master/CHANGELOG.md).
