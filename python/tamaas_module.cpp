/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "tamaas.hh"
#include "tamaas_info.hh"
#include "wrap.hh"
/* -------------------------------------------------------------------------- */
#include <pybind11/pybind11.h>
/* -------------------------------------------------------------------------- */

namespace tamaas {
namespace py = pybind11;

namespace detail {
template <typename T>
struct dtype_helper {
  static const py::dtype dtype;
};

template <>
const py::dtype dtype_helper<double>::dtype("=f8");
template <>
const py::dtype dtype_helper<long double>::dtype("=f16");
}  // namespace detail

/// Creating the tamaas python module
PYBIND11_MODULE(_tamaas, mod) {
  mod.doc() = "Compiled component of Tamaas";

  // Wrapping the base methods
  mod.def("initialize", &initialize, py::arg("num_threads") = 0,
          "Initialize tamaas with desired number of threads. Automatically "
          "called upon import of the tamaas module, but can be manually called "
          "to set the desired number of threads.");
  mod.def("finalize", []() {
    PyErr_WarnEx(PyExc_DeprecationWarning,
                 "finalize() is deprecated, it is now automatically called "
                 "at the end of the program",
                 1);
  });

  // Default dtype of numpy arrays
  mod.attr("dtype") = detail::dtype_helper<Real>::dtype;

  // Wrapping release information
  auto info = py::class_<TamaasInfo>(mod, "TamaasInfo");
  info.attr("version") = TamaasInfo::version;
  info.attr("build_type") = TamaasInfo::build_type;
  info.attr("branch") = TamaasInfo::branch;
  info.attr("commit") = TamaasInfo::commit;
  info.attr("diff") = TamaasInfo::diff;
  info.attr("remotes") = TamaasInfo::remotes;
  info.attr("has_mpi") = TamaasInfo::has_mpi;
  info.attr("has_petsc") = TamaasInfo::has_petsc;
  info.attr("backend") = TamaasInfo::backend;

  // Wrapping tamaas components
  wrap::wrapCore(mod);
  wrap::wrapPercolation(mod);
  wrap::wrapSurface(mod);
  wrap::wrapModel(mod);
  wrap::wrapSolvers(mod);
  wrap::wrapCompute(mod);
  wrap::wrapMPI(mod);
  wrap::wrapMaterials(mod);

  /// Wrapping test features
  wrap::wrapTestFeatures(mod);

  // Translate exceptions to python
  py::register_exception<assertion_error>(mod, "AssertionError",
                                          PyExc_AssertionError);
  py::register_exception<not_implemented_error>(mod, "NotImplementedError",
                                                PyExc_NotImplementedError);
  py::register_exception<model_type_error>(mod, "ModelTypeError",
                                           PyExc_TypeError);
  py::register_exception<nan_error>(mod, "FloatingPointError",
                                    PyExc_FloatingPointError);
}

}  // namespace tamaas
