/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "boussinesq.hh"
#include "kelvin.hh"
#include "materials/isotropic_hardening.hh"
#include "mindlin.hh"
#include "model.hh"
#include "model_type.hh"
#include "volume_potential.hh"
#include "wrap.hh"
/* -------------------------------------------------------------------------- */

namespace tamaas {

namespace wrap {

using namespace py::literals;

/// Wrap temporary features for testing
void wrapTestFeatures(py::module& mod) {
  auto test_module = mod.def_submodule("_test_features");
  test_module.doc() =
      "Module for testing new features.\n"
      "DISCLAIMER: this API is subject to frequent and unannounced changes "
      "and should **not** be relied upon!";
}
}  // namespace wrap

}  // namespace tamaas
