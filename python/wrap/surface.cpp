/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "isopowerlaw.hh"
#include "regularized_powerlaw.hh"
#include "surface_generator_filter.hh"
#include "surface_generator_random_phase.hh"
#include "wrap.hh"
#include <pybind11/stl.h>
/* -------------------------------------------------------------------------- */

namespace tamaas {

/* -------------------------------------------------------------------------- */
namespace wrap {

using namespace py::literals;
/* -------------------------------------------------------------------------- */

template <UInt dim>
class PyFilter : public Filter<dim> {
public:
  using Filter<dim>::Filter;

  // Overriding pure virtual functions
  void
  computeFilter(GridHermitian<Real, dim>& filter_coefficients) const override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERLOAD_PURE(void, Filter<dim>, computeFilter,
                           filter_coefficients);
  }
};

template <UInt dim>
void wrapFilter(py::module& mod) {
  auto name = makeDimensionName("Filter", dim);
  py::class_<Filter<dim>, std::shared_ptr<Filter<dim>>, PyFilter<dim>>(
      mod, name.c_str(), "Mother class for Fourier filter objects")
      .def(py::init<>())
      .def("computeFilter",
           (void (Filter<dim>::*)(GridHermitian<Real, dim>&) const) &
               Filter<dim>::computeFilter,
           "Compute the Fourier coefficient of the surface");
}

/* -------------------------------------------------------------------------- */
template <UInt dim>
void wrapIsopowerlaw(py::module& mod) {
  std::string name = makeDimensionName("Isopowerlaw", dim);
  py::class_<Isopowerlaw<dim>, Filter<dim>, std::shared_ptr<Isopowerlaw<dim>>>(
      mod, name.c_str(), "Isotropic powerlaw spectrum with a rolloff plateau")
      .def(py::init<>())
      .def_property("q0", &Isopowerlaw<dim>::getQ0, &Isopowerlaw<dim>::setQ0,
                    "Long wavelength cutoff")
      .def_property("q1", &Isopowerlaw<dim>::getQ1, &Isopowerlaw<dim>::setQ1,
                    "Rolloff wavelength")
      .def_property("q2", &Isopowerlaw<dim>::getQ2, &Isopowerlaw<dim>::setQ2,
                    "Short wavelength cutoff")
      .def_property("hurst", &Isopowerlaw<dim>::getHurst,
                    &Isopowerlaw<dim>::setHurst, "Hurst exponent")
      .def("rmsHeights", &Isopowerlaw<dim>::rmsHeights,
           "Theoretical RMS of heights")
      .def("moments", &Isopowerlaw<dim>::moments,
           "Theoretical first 3 moments of spectrum")
      .def("alpha", &Isopowerlaw<dim>::alpha, "Nayak's bandwidth parameter")
      .def("rmsSlopes", &Isopowerlaw<dim>::rmsSlopes,
           "Theoretical RMS of slopes")
      .def("radialPSDMoment", &Isopowerlaw<dim>::radialPSDMoment,
           "Computes ∫ k^q ɸ(k) k dk from 0 to ∞")
      .def("elasticEnergy", &Isopowerlaw<dim>::elasticEnergy,
           "Computes full contact energy (adimensional)");

  name = makeDimensionName("RegularizedPowerlaw", dim);
  py::class_<RegularizedPowerlaw<dim>, Filter<dim>,
             std::shared_ptr<RegularizedPowerlaw<dim>>>(
      mod, name.c_str(),
      "Isotropic regularized powerlaw with a plateau extending to the size of "
      "the system")
      .def(py::init<>())
      .def_property("q1", &RegularizedPowerlaw<dim>::getQ1,
                    &RegularizedPowerlaw<dim>::setQ1, "Rolloff wavelength")
      .def_property("q2", &RegularizedPowerlaw<dim>::getQ2,
                    &RegularizedPowerlaw<dim>::setQ2, "Short wavelength cutoff")
      .def_property("hurst", &RegularizedPowerlaw<dim>::getHurst,
                    &RegularizedPowerlaw<dim>::setHurst, "Hurst exponent");
}

/* -------------------------------------------------------------------------- */
template <UInt dim>
void wrapSurfaceGenerators(py::module& mod) {
  std::string generator_name = makeDimensionName("SurfaceGenerator", dim);
  py::class_<SurfaceGenerator<dim>>(mod, generator_name.c_str())
      .def("buildSurface", &SurfaceGenerator<dim>::buildSurface,
           py::return_value_policy::reference_internal,
           "Generate a surface and return a reference to it")
      .def("setSizes",
           [](SurfaceGenerator<dim>& m, std::array<UInt, dim> s) {
             TAMAAS_DEPRECATE("setSizes()", "the shape property");
             m.setSizes(std::move(s));
           })
      .def("setRandomSeed",
           [](SurfaceGenerator<dim>& m, long s) {
             TAMAAS_DEPRECATE("setRandomSeed()", "the random_seed property");
             m.setRandomSeed(s);
           })

      .def_property("random_seed", &SurfaceGenerator<dim>::getRandomSeed,
                    &SurfaceGenerator<dim>::setRandomSeed,
                    "Random generator seed")
      .def_property("shape", &SurfaceGenerator<dim>::getSizes,
                    py::overload_cast<std::array<UInt, dim>>(
                        &SurfaceGenerator<dim>::setSizes),
                    "Global shape of surfaces");

  std::string filter_name = makeDimensionName("SurfaceGeneratorFilter", dim);
  py::class_<SurfaceGeneratorFilter<dim>, SurfaceGenerator<dim>>(
      mod, filter_name.c_str(),
      "Generates a rough surface with Gaussian noise in the PSD")
      .def(py::init<>(), "Default constructor")
      .def(py::init<std::array<UInt, dim>>(),
           "Initialize with global surface shape")

      .def(
          "setFilter",
          [](SurfaceGeneratorFilter<dim>& m, std::shared_ptr<Filter<dim>> s) {
            TAMAAS_DEPRECATE("setFilter()", "the spectrum property");
            m.setSpectrum(std::move(s));
          },
          "Set PSD filter", "filter"_a)
      .def(
          "setSpectrum",
          [](SurfaceGeneratorFilter<dim>& m, std::shared_ptr<Filter<dim>> s) {
            TAMAAS_DEPRECATE("setSpectrum()", "the spectrum property");
            m.setSpectrum(std::move(s));
          },
          "Set PSD filter", "filter"_a)

      .def_property("spectrum", &SurfaceGeneratorFilter<dim>::getSpectrum,
                    &SurfaceGeneratorFilter<dim>::setSpectrum,
                    "Power spectrum object");

  std::string random_name =
      makeDimensionName("SurfaceGeneratorRandomPhase", dim);
  py::class_<SurfaceGeneratorRandomPhase<dim>, SurfaceGeneratorFilter<dim>>(
      mod, random_name.c_str(),
      "Generates a rough surface with uniformly distributed phases and exact "
      "prescribed PSD")
      .def(py::init<>(), "Default constructor")
      .def(py::init<std::array<UInt, dim>>(),
           "Initialize with global surface shape");
}

/* -------------------------------------------------------------------------- */
void wrapSurface(py::module& mod) {
  wrapFilter<1>(mod);
  wrapFilter<2>(mod);
  wrapIsopowerlaw<1>(mod);
  wrapIsopowerlaw<2>(mod);
  wrapSurfaceGenerators<1>(mod);
  wrapSurfaceGenerators<2>(mod);
}

}  // namespace wrap
/* -------------------------------------------------------------------------- */
}  // namespace tamaas
