/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "functional.hh"
#include "model.hh"
#include "model_dumper.hh"
#include "residual.hh"
#include "wrap.hh"
#include <pybind11/pybind11.h>
/* -------------------------------------------------------------------------- */
namespace tamaas {
/* -------------------------------------------------------------------------- */

namespace functional {
namespace wrap {
/// Class for extension of Functional in python
class PyFunctional : public Functional {
public:
  using Functional::Functional;

  // Overriding pure virtual functions
  Real computeF(GridBase<Real>& variable, GridBase<Real>& dual) const override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERLOAD_PURE(Real, Functional, computeF, variable, dual);
  }

  void computeGradF(GridBase<Real>& variable,
                    GridBase<Real>& gradient) const override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERLOAD_PURE(void, Functional, computeGradF, variable, gradient);
  }
};
}  // namespace wrap
}  // namespace functional
/* -------------------------------------------------------------------------- */
namespace wrap {

/* -------------------------------------------------------------------------- */
class PyModelDumper : public ModelDumper {
public:
  using ModelDumper::ModelDumper;

  void dump(const Model& model) override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERLOAD_PURE(void, ModelDumper, dump, model);
  }
};

/* -------------------------------------------------------------------------- */
class PyResidual : public Residual {
public:
  using Residual::Residual;

  void computeResidual(GridBase<Real>& strain_increment) override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERLOAD(void, Residual, computeResidual, strain_increment);
  }

  void updateState(GridBase<Real>& converged_strain_increment) override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERLOAD(void, Residual, updateState, converged_strain_increment);
  }

  void computeResidualDisplacement(GridBase<Real>& strain_increment) override {
    // NOLINTNEXTLINE(readability-else-after-return)
    PYBIND11_OVERLOAD(void, Residual, computeResidualDisplacement,
                      strain_increment);
  }
};

class PyIntegralOperator : public IntegralOperator {
public:
  using IntegralOperator::IntegralOperator;

  void apply(GridBase<Real>& input, GridBase<Real>& output) const override {
    PYBIND11_OVERLOAD_PURE(void, IntegralOperator, apply, input, output);
  }

  IntegralOperator::kind getKind() const override {
    PYBIND11_OVERLOAD(IntegralOperator::kind, IntegralOperator, getKind);
  }

  model_type getType() const override {
    PYBIND11_OVERLOAD(model_type, IntegralOperator, getType);
  }
};

/* -------------------------------------------------------------------------- */

}  // namespace wrap
/* -------------------------------------------------------------------------- */
}  // namespace tamaas
