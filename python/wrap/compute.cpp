/*
 *  SPDX-License-Indentifier: AGPL-3.0-or-later
 *
 *  Copyright (©) 2016-2024 EPFL (École Polytechnique Fédérale de Lausanne),
 *  Laboratory (LSMS - Laboratoire de Simulation en Mécanique des Solides)
 *  Copyright (©) 2020-2024 Lucas Frérot
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
/* -------------------------------------------------------------------------- */
#include "computes.hh"
#include "wrap.hh"
/* -------------------------------------------------------------------------- */

namespace tamaas {
namespace wrap {

using namespace py::literals;

void wrapCompute(py::module& mod) {
  auto compute_mod = mod.def_submodule("compute");
  compute_mod.doc() = "Module defining basic computations on fields.";
  compute_mod.def(
      "eigenvalues",
      [](model_type type, Grid<Real, 3>& eigs, const Grid<Real, 3>& field) {
        eigenvalues(type, eigs, field);
      },
      "model_type"_a, "eigenvalues_out"_a, "field"_a,
      "Compute eigenvalues of a tensor field");
  compute_mod.def(
      "von_mises",
      [](model_type type, Grid<Real, 3>& vm, const Grid<Real, 3>& field) {
        vonMises(type, vm, field);
      },
      "model_type"_a, "von_mises"_a, "field"_a,
      "Compute the Von Mises invariant of a tensor field");
  compute_mod.def(
      "deviatoric",
      [](model_type type, Grid<Real, 3>& dev, const Grid<Real, 3>& field) {
        deviatoric(type, dev, field);
      },
      "model_type"_a, "deviatoric"_a, "field"_a,
      "Compute the deviatoric part of a tensor field");

  compute_mod.def(
      "to_voigt",
      [](const Grid<Real, 3>& field) {
        TAMAAS_ASSERT(field.getNbComponents() == 9,
                      "Wrong number of components to symmetrize");

        Grid<Real, 3> voigt(field.sizes(), 6);
        Loop::loop(
            [] CUDA_LAMBDA(MatrixProxy<const Real, 3, 3> in,
                           SymMatrixProxy<Real, 3> out) { out.symmetrize(in); },
            range<MatrixProxy<const Real, 3, 3>>(field),
            range<SymMatrixProxy<Real, 3>>(voigt));
        return voigt;
      },
      "Convert a 3D tensor field to voigt notation",
      py::return_value_policy::copy);

  compute_mod.def(
      "from_voigt",
      [](const Grid<Real, 3>& field) {
        TAMAAS_ASSERT(field.getNbComponents() == 6,
                      "Wrong number of components to symmetrize");

        Grid<Real, 3> full(field.sizes(), 9);
        Loop::loop([] CUDA_LAMBDA(
                       SymMatrixProxy<const Real, 3> in,
                       MatrixProxy<Real, 3, 3> out) { out.fromSymmetric(in); },
                   range<SymMatrixProxy<const Real, 3>>(field),
                   range<MatrixProxy<Real, 3, 3>>(full));
        return full;
      },
      "Convert a 3D tensor field to voigt notation",
      py::return_value_policy::copy);
}
}  // namespace wrap
}  // namespace tamaas
